Some computer graphics algorithms.

Runs with Java 1.7 and needs [jung](http://jung.sourceforge.net/)

LICENSE of the Implementation is: [GPL-2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)