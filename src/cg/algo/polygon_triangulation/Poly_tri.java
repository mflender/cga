/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.polygon_triangulation;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import cg.Drawing_Space;
import cg.algo.Algo;
import cg.algo.scanline.Circular_double_linked_list;
import cg.drawable.Colored_line;
import cg.drawable.Colored_point;
import cg.drawable.Colored_polygon;
import cg.drawable.Colored_polygon_self;
import cg.drawable.Draw_Container;

/**
 * Performs a polygon triangulation. Works only with CCW sortet polygons
 * 
 * @author malte
 * 
 */
public class Poly_tri extends Algo {
	private TreeMap<Colored_line, Colored_point> search_tree = null;
	private Drawing_Space panel = null;

	// Green
	private final int START_VERTEX_R = 0;
	private final int START_VERTEX_G = 255;
	private final int START_VERTEX_B = 0;

	// Blue
	private final int SPLIT_VERTEX_R = 0;
	private final int SPLIT_VERTEX_G = 0;
	private final int SPLIT_VERTEX_B = 255;

	// Red
	private final int END_VERTEX_R = 255;
	private final int END_VERTEX_G = 0;
	private final int END_VERTEX_B = 0;

	// Purple
	private final int MERGE_VERTEX_R = 255;
	private final int MERGE_VERTEX_G = 0;
	private final int MERGE_VERTEX_B = 255;

	// Yellow/Orange
	private final int LINE_R = 236;
	private final int LINE_G = 150;
	private final int LINE_B = 20;

	private final int WAIT_TIME = 200;
	private Colored_point actual = null;

	private static enum Turn {
		CLOCKWISE, COUNTER_CLOCKWISE, COLLINEAR
	}

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {
		if (bad_input(data)) {
			return;
		} else {
			data.get_lines().clear();
			data.get_points().clear();
			data.get_polygons().clear();
			this.panel = draw_panel;
		}

		color_points(data.get_polygons_self().get(0));

		wait_paint(draw_panel, WAIT_TIME * 2);
		make_polygon_y_monotone(data);

		wait_paint(draw_panel, WAIT_TIME * 2);
		swap_lines_to_new_Polygon(data.get_polygons_self());

		for (Colored_polygon_self polygon : data.get_polygons_self()) {
			triangulate_y_monotone_polygon(polygon);
			wait_paint(draw_panel, WAIT_TIME);
		}
//		for (Colored_polygon_self polygon : data.get_polygons_self()) {
//			polygon.set_color(new Color(0));
//		}
	}

	/**
	 * Transforms a given polygon into a set of y monotone polygons
	 * 
	 * @param data
	 *            the data object, that stores the polygons
	 */
	private void make_polygon_y_monotone(Draw_Container data) {
		List<Colored_point> queue = sort_points_y(data.get_polygons_self()
				.get(0).get_points());

		init_tree();

		for (Colored_point point : queue) {
			actual = point;
			if (is_start_vertex(point)) {
				start_vertex_event(point);
			} else if (is_split_vertex(point)) {
				split_vertex_event(point, data.get_polygons_self());
			} else if (is_end_vertex(point)) {
				end_vertex_event(point, data.get_polygons_self());
			} else if (is_merge_vertex(point)) {
				merge_vertex_event(point, data.get_polygons_self());
			} else if (is_regular_vertex(point)) {
				regular_vertex_event(point, data.get_polygons_self());
			} else {
				System.err
						.println("Poly_tri.make_polygon_y_monoton: unkowen case.");
			}
			wait_paint(panel, WAIT_TIME);
		}
	}

	/**
	 * performs a regular-vertex-event according to the algorithm
	 * 
	 * @param point
	 *            the point to perform the action on.
	 * 
	 * @param list
	 *            the list of polygons to create a new polygon
	 * 
	 */
	private void regular_vertex_event(Colored_point point,
			List<Colored_polygon_self> list) {
		if (interior_is_right(point, list.get(0))) {

			Colored_line e_i_minus_1 = point.get_line_end_here();
			Colored_line e_i = point.get_line_start_here();
			Colored_point helper_e_i_minus_1 = search_tree.get(e_i_minus_1);

			if (is_merge_vertex(helper_e_i_minus_1)) {
				create_new_polygon(point, list, helper_e_i_minus_1);
			}
			search_tree.remove(e_i_minus_1);
			search_tree.put(e_i, point);
		} else {
			Colored_line e_j = get_left_of(point);
			Colored_point helper_e_j = search_tree.get(e_j);

			if (is_merge_vertex(helper_e_j)) {
				create_new_polygon(point, list, helper_e_j);
			}
			search_tree.remove(e_j);
			search_tree.put(e_j, point);
		}
	}

	/**
	 * performs a merge-vertex-event according to the algorithm
	 * 
	 * @param point
	 *            the point to perform the action on.
	 * @param list
	 *            the list of polygons to create a new polygon
	 * 
	 */
	private void merge_vertex_event(Colored_point point,
			List<Colored_polygon_self> list) {
		Colored_line e_i_minus_1 = point.get_line_end_here();
		Colored_point helper_e_i_minus_1 = search_tree.get(e_i_minus_1);

		if (is_merge_vertex(helper_e_i_minus_1)) {
			create_new_polygon(point, list, helper_e_i_minus_1);
		}

		search_tree.remove(e_i_minus_1);
		Colored_line e_j = get_left_of(point);
		Colored_point helper_e_j = search_tree.get(e_j);

		if (is_merge_vertex(helper_e_j)) {
			create_new_polygon(point, list, helper_e_j);
		}

		search_tree.remove(e_j);
		helper_e_j = point;
		search_tree.put(e_j, helper_e_j);

	}

	/**
	 * performs a end-vertex-event according to the algorithm
	 * 
	 * @param point
	 *            the point to perform the action on.
	 * @param list
	 *            the list of polygons to create a new polygon
	 * 
	 */
	private void end_vertex_event(Colored_point point,
			List<Colored_polygon_self> list) {
		Colored_line e_i_minus_1 = point.get_line_end_here();
		Colored_point helper_e_i_minus_1 = search_tree.get(e_i_minus_1);

		if (is_merge_vertex(helper_e_i_minus_1)) {
			create_new_polygon(point, list, helper_e_i_minus_1);
		}

		search_tree.remove(e_i_minus_1);

	}

	/**
	 * performs a split-vertex-event according to the algorithm
	 * 
	 * @param point
	 *            the point to perform the action on.
	 * @param list
	 *            the list of polygons to create a new polygon
	 * 
	 */
	private void split_vertex_event(Colored_point point,
			List<Colored_polygon_self> list) {
		Colored_line e_j = get_left_of(point);
		Colored_line e_i = point.get_line_start_here();

		Colored_point helper_e_j = search_tree.get(e_j);

		create_new_polygon(point, list, helper_e_j);

		search_tree.remove(e_j);
		search_tree.put(e_j, point);

		search_tree.put(e_i, point);

	}

	/**
	 * performs a start-vertex-event according to the algorithm
	 * 
	 * @param point
	 *            the point to perform the action on.
	 * 
	 */
	private void start_vertex_event(Colored_point point) {
		search_tree.put(point.get_line_start_here(), point);
	}

	/**
	 * Creates a new y-monotone polygon from the given points and the old
	 * polygon.
	 * 
	 * @param start_point
	 *            the start point of the new line
	 * @param list
	 *            the end point of the new line
	 * @param end_point
	 *            the polygon containing the old lines
	 */
	private void create_new_polygon(Colored_point start_point,
			List<Colored_polygon_self> list, Colored_point end_point) {
		Colored_polygon_self new_polygon = new Colored_polygon_self();
		Colored_line line = new Colored_line(start_point, end_point, new Color(
				LINE_R, LINE_G, LINE_B));
		new_polygon.get_lines().add(line);
		new_polygon.get_points().add(start_point);
		new_polygon.get_points().add(end_point);
		list.add(new_polygon);
	}

	/**
	 * triangulates a given y monotone polygon.
	 * 
	 * @param polygon
	 *            the polygon to triangulate
	 */
	private void triangulate_y_monotone_polygon(Colored_polygon_self polygon) {
		List<Colored_point> queue = sort_points_y(polygon.get_points());
		List<Colored_point> tmp_same_side = new ArrayList<>();
		Stack<Colored_point> stack = new Stack<>();
		stack.push(queue.get(0));
		stack.push(queue.get(1));
		for (int j = 2; j < queue.size() - 1; j++) {
			wait_paint(panel, WAIT_TIME);
			Colored_point a = queue.get(j);
			Colored_point b = stack.peek();
			if (different_sides(a, b, polygon, queue)) {
				while (stack.size() > 1) {
					Colored_point point = stack.pop();
					Colored_line line = new Colored_line(queue.get(j), point,
							new Color(LINE_R, LINE_G, LINE_B));
					polygon.get_lines().add(line);
				}
				stack.pop();
				stack.push(queue.get(j - 1));
				stack.push(queue.get(j));
			} else {
				tmp_same_side.add(stack.pop());
				while (!stack.empty()) {
					Colored_point point = stack.pop();
					tmp_same_side.add(point);
					Colored_line line = new Colored_line(queue.get(j), point,
							new Color(LINE_R, LINE_G, LINE_B));
					if (no_intersection(line, polygon.get_lines())
							&& line_lies_in_polygon(line, polygon)) {
						polygon.get_lines().add(line);
						tmp_same_side.remove(tmp_same_side.size() - 2);
					}
				}
				Collections.reverse(tmp_same_side);
				stack.addAll(tmp_same_side);
				stack.push(queue.get(j));
				tmp_same_side.clear();
			}
		}
		stack.pop();
		while (stack.size() > 1) {
			Colored_point point = stack.pop();
			Colored_line line = new Colored_line(queue.get(queue.size() - 1),
					point, new Color(LINE_R, LINE_G, LINE_B));
			polygon.get_lines().add(line);
		}
	}

	/**
	 * Checks if a given line lies inside the given polygon.
	 * 
	 * @param line
	 *            the line to check
	 * @param polygon
	 *            the polygon to check
	 * @return true if the line lies inside the polygon
	 */
	private boolean line_lies_in_polygon(Colored_line line,
			Colored_polygon_self polygon) {
		Colored_polygon poly = polygon.get_poly_rep();
		boolean intersection = false;
		float lambda = 0.0f;

		while (lambda < 1.0) {
			int temp_x = (int) ((1 - lambda) * line.x1 + lambda * line.x2);
			int temp_y = (int) ((1 - lambda) * line.y1 + lambda * line.y2);
			lambda += 0.1;

			if (poly.contains(temp_x, temp_y)
					&& (temp_x != line.x1 || temp_y != line.y1)) {
				intersection = true;
				return intersection;
			}
		}
		return intersection;
	}

	/**
	 * Checks if the given line has no intersection with the lines from the
	 * polygon.
	 * 
	 * @param line
	 *            the line to check
	 * @param lines
	 *            the list of lines to check
	 * @return true if there is no intersection false otherwise
	 */
	private boolean no_intersection(Colored_line line, List<Colored_line> lines) {
		for (Colored_line line_tmp : lines) {
			if (line.intersectsLine(line_tmp)) {
				if (!line.getP1().equals(line_tmp.getP1())
						&& !line.getP2().equals(line_tmp.getP2())
						&& !line.getP1().equals(line_tmp.getP2())
						&& !line.getP2().equals(line_tmp.getP1())) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Checks if two points lie on different sides of the given polygon.
	 * 
	 * @param point_a
	 *            the first point to check
	 * @param point_b
	 *            the second point to check
	 * @param polygon
	 *            the polygon to determine the position
	 * @param queue
	 *            the sorted points
	 * @return true if the point lie on different sides
	 */
	private boolean different_sides(Colored_point point_a,
			Colored_point point_b, Colored_polygon_self polygon,
			List<Colored_point> queue) {
		Colored_point highest = queue.get(0);
		Colored_point lowest = queue.get(queue.size() - 1);
		Colored_point actual = highest;
		ArrayList<Colored_point> left_side = new ArrayList<>();

		while (actual != lowest) {
			left_side.add(actual);
			Colored_line start = actual.get_line_start_here();
			actual = polygon.get_end_point_from_line(start);
		}

		if (left_side.contains(point_a) ^ left_side.contains(point_b)) { // XOR
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if the inside of the polygon is on the right side of the point.
	 * 
	 * @param point
	 *            the point to check
	 * @param polygon
	 *            the polygon witch the point is adjacent to
	 * @return true if the inside of the polygon is on the right side of the
	 *         point
	 */
	private boolean interior_is_right(Colored_point point,
			Colored_polygon_self polygon) {
		Colored_point before = polygon.get_start_point_from_line(point
				.get_line_end_here());

		if (before.getY() >= point.y) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Finds the line in search_tree witch is left of the point
	 * 
	 * @param point
	 *            the point to check
	 * @return the line left of the point or null
	 */
	private Colored_line get_left_of(Colored_point point) {
		Colored_line reference = new Colored_line(point.getX(), panel.getY(),
				point.getX(), panel.getHeight(), new Color(0));
		Colored_line nearest_line = search_tree.floorKey(reference);

		// Colored_line nearest_line = null;
		// int nearest_x = panel.getWidth();
		// for (Map.Entry<Colored_line, Colored_point> entry :
		// search_tree.entrySet()) {
		// Colored_line line_tmp = entry.getKey();
		//
		// int diff_x = (int) line_tmp.ptLineDist(point);
		//
		// if ((diff_x <= nearest_x && diff_x >= 0)
		// && (line_tmp.y2 < point.y && point.y < line_tmp.y1)) {
		// nearest_x = diff_x;
		// nearest_line = line_tmp;
		// }
		// }
		return nearest_line;
	}

	/**
	 * Creates a new search tree to store the helper in.
	 */
	private void init_tree() {
		search_tree = new TreeMap<>(new Comparator<Colored_line>() {
			@Override
			public int compare(Colored_line a, Colored_line b) {

				double dist_a = a.ptSegDist(actual);
				double dist_b = b.ptSegDist(actual);
				int direction_a = a.relativeCCW(actual);
				int direction_b = b.relativeCCW(actual);

				if (direction_a < 0) {
					dist_a = dist_a * (-1);
				}
				if (direction_b < 0) {
					dist_b = dist_b * (-1);
				}

				if (a.equals(b) || a == b) {
					return 0;
				} else if (dist_a < dist_b) {
					return -1;
				} else {
					return 1;
				}

				// int a_x = (int) ((a.x1 + a.x2) / 2);
				// int b_x = (int) ((b.x1 + b.x2) / 2);
				// int a_y = (int) ((a.y1 + a.y2) / 2);
				// int b_y = (int) ((b.y1 + b.y2) / 2);
				//
				// if (a.equals(b) || a == b) {
				// return 0;
				// } else if ((a_y >= b_y) || (a_y == b_y && a_x > b_x)) {
				// return -1;
				// } else {
				// return 1;
				// }
			}
		});
	}

	/**
	 * Checks if a given point is a regular vertex
	 * 
	 * @param point
	 *            the point to check
	 * @return true if it is a regular vertex, else false
	 */
	private boolean is_regular_vertex(Colored_point point) {
		if (point.get_color().getRed() == 0
				&& point.get_color().getGreen() == 0
				&& point.get_color().getBlue() == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if a given point is a merge vertex
	 * 
	 * @param point
	 *            the point to check
	 * @return true if it is a merge vertex, else false
	 */
	private boolean is_merge_vertex(Colored_point point) {
		if (point.get_color().getRed() == MERGE_VERTEX_R
				&& point.get_color().getGreen() == MERGE_VERTEX_G
				&& point.get_color().getBlue() == MERGE_VERTEX_B) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if a given point is a end vertex
	 * 
	 * @param point
	 *            the point to check
	 * @return true if it is a end vertex, else false
	 */
	private boolean is_end_vertex(Colored_point point) {
		if (point.get_color().getRed() == END_VERTEX_R
				&& point.get_color().getGreen() == END_VERTEX_G
				&& point.get_color().getBlue() == END_VERTEX_B) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Checks if a given point is a split vertex
	 * 
	 * @param point
	 *            the point to check
	 * @return true if it is a split vertex, else false
	 */
	private boolean is_split_vertex(Colored_point point) {
		if (point.get_color().getRed() == SPLIT_VERTEX_R
				&& point.get_color().getGreen() == SPLIT_VERTEX_G
				&& point.get_color().getBlue() == SPLIT_VERTEX_B) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if a given point is a start vertex
	 * 
	 * @param point
	 *            the point to check
	 * @return true if it is a start vertex, else false
	 */
	private boolean is_start_vertex(Colored_point point) {
		if (point.get_color().getRed() == START_VERTEX_R
				&& point.get_color().getGreen() == START_VERTEX_G
				&& point.get_color().getBlue() == START_VERTEX_B) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sorts all points in y direction.
	 * 
	 * @param points
	 *            the points to sort
	 * @return the sorted points
	 */
	private List<Colored_point> sort_points_y(List<Colored_point> points) {
		List<Colored_point> y_sort_tmp = new ArrayList<>();
		TreeSet<Colored_point> sort_set = new TreeSet<>(
				new Comparator<Colored_point>() {
					@Override
					public int compare(Colored_point a, Colored_point b) {
						if (a.equals(b) || a == b) {
							return 0;
						} else if (a.y > b.y) {
							return -1;
						} else {
							return 1;
						}
					}
				});
		sort_set.addAll(points);
		y_sort_tmp.addAll(sort_set);
		return y_sort_tmp;
	}

	/**
	 * Colors the points of a polygon in different colors regarding to its
	 * relative position
	 * 
	 * @param polygon
	 *            the polygon to be colored
	 */
	private void color_points(Colored_polygon_self polygon) {
		Circular_double_linked_list<Colored_point> poly_list = new Circular_double_linked_list<>();
		poly_list.addAll(polygon.get_points());

		for (int i = 0; i < poly_list.size(); i++) {
			color_point(poly_list.get_predecessor(poly_list.get(i)),
					poly_list.get(i), poly_list.get_successor(poly_list.get(i)));
			wait_paint(panel, WAIT_TIME);
		}
	}

	/**
	 * Classifies a point with the relative position to its direct neighbors and
	 * colors it.
	 * 
	 * If the point is a start vertex of the scanline it is colored in green. If
	 * the point is a split vertex of the scanline it is colored in blue. If the
	 * point is a end vertex of the scanline it is colored in red. If the point
	 * is a merge vertex of the scanline it is colored in purple. If the point
	 * is a regular vertex of the scanline it is colored in black.
	 * 
	 * @param before
	 *            the predecessor of the point
	 * @param actual
	 *            the point to classify
	 * @param after
	 *            the successor of the point
	 */
	private void color_point(Colored_point before, Colored_point actual,
			Colored_point after) {
		if (is_below(actual, before) && is_below(actual, after)) {
			if (get_Turn(before, actual, after) == Turn.COUNTER_CLOCKWISE) {
				actual.set_color(new Color(START_VERTEX_R, START_VERTEX_G,
						START_VERTEX_B));
			} else if (get_Turn(before, actual, after) == Turn.CLOCKWISE) {
				actual.set_color(new Color(SPLIT_VERTEX_R, SPLIT_VERTEX_G,
						SPLIT_VERTEX_B));
			}
		} else if (is_above(actual, before) && is_above(actual, after)) {
			if (get_Turn(before, actual, after) == Turn.COUNTER_CLOCKWISE) {
				actual.set_color(new Color(END_VERTEX_R, END_VERTEX_G,
						END_VERTEX_B));
			} else if (get_Turn(before, actual, after) == Turn.CLOCKWISE) {
				actual.set_color(new Color(MERGE_VERTEX_R, MERGE_VERTEX_G,
						MERGE_VERTEX_B));
			}
		}
	}

	/**
	 * Checks if a point is above a reference point.
	 * 
	 * @param ref
	 *            the reference point
	 * @param point
	 *            the point to check
	 * @return true if the point is above else false
	 */
	private boolean is_above(Colored_point ref, Colored_point point) {
		boolean above = false;
		if (point.y > ref.y || (point.y == ref.y && point.x < ref.x)) {
			above = true;
		}
		return above;
	}

	/**
	 * Checks if a point is below a reference point.
	 * 
	 * @param ref
	 *            the reference point
	 * @param point
	 *            the point to check
	 * @return true if the point is below else false
	 */
	private boolean is_below(Colored_point ref, Colored_point point) {
		boolean below = false;
		if (point.y < ref.y || (point.y == ref.y && point.x > ref.x)) {
			below = true;
		}
		return below;
	}

	/**
	 * Crates a set of y-monotone polygons.
	 * 
	 * @param polygons
	 *            the unfinished list of polygons
	 */
	private void swap_lines_to_new_Polygon(List<Colored_polygon_self> polygons) {
		Colored_polygon_self first_polygon = polygons.get(0);
		for (int i = 1; i < polygons.size(); i++) {
			Colored_polygon_self actual_polygon = polygons.get(i);
			Colored_line cut_line = actual_polygon.get_lines().get(0);
			Colored_point start_point = actual_polygon
					.get_start_point_from_line(cut_line);
			Colored_point end_point = actual_polygon
					.get_end_point_from_line(cut_line);

			actual_polygon.get_points().clear();
			actual_polygon.get_lines().clear();

			if (sub_polygon_is_left(start_point, end_point, first_polygon)) {
				Colored_point actual_point = end_point;
				actual_polygon.get_points().add(end_point);
				while (actual_point != start_point) {
					actual_point = first_polygon
							.get_end_point_from_line(actual_point
									.get_line_start_here());
					actual_point.set_Used(true);
					actual_polygon.get_points().add(actual_point);
				}
			} else {
				Colored_point actual_point = start_point;
				actual_polygon.get_points().add(start_point);
				while (actual_point != end_point) {
					actual_point = first_polygon
							.get_end_point_from_line(actual_point
									.get_line_start_here());
					actual_point.set_Used(true);
					actual_polygon.get_points().add(actual_point);
				}
			}
			start_point.set_Used(false);
			end_point.set_Used(false);
			actual_polygon.decuple_from_old(start_point, end_point);
			actual_polygon.set_color(new Color(0, 0, 0));
			actual_polygon.create_lines();

			first_polygon.remove_used();
			first_polygon.get_lines().clear();
			first_polygon.clear_point_pointer();
			first_polygon.set_color(new Color(0, 0, 0));
			first_polygon.create_lines();
		}
		first_polygon.remove_used();
		first_polygon.get_lines().clear();
		first_polygon.clear_point_pointer();
		first_polygon.set_color(new Color(0, 0, 0));
		first_polygon.create_lines();
	}

	/**
	 * Checks if the sub polygon is on the left or right side of the polygon.
	 * 
	 * @param start_point
	 *            the Start point of the cut line
	 * @param end_point
	 *            the end point of the cut line
	 * @param first_polygon
	 *            the polygon to get the vertex list from
	 * @return true if the polygon lies on the left side else false
	 */
	private boolean sub_polygon_is_left(Colored_point start_point,
			Colored_point end_point, Colored_polygon_self first_polygon) {
		int count_left = 0;
		int count_rigt = 0;

		Colored_point actual_point = start_point;
		while (actual_point != end_point) {
			actual_point = first_polygon.get_start_point_from_line(actual_point
					.get_line_end_here());
			count_left++;
		}

		actual_point = start_point;
		while (actual_point != end_point) {
			actual_point = first_polygon.get_end_point_from_line(actual_point
					.get_line_start_here());
			count_rigt++;
		}

		if (count_left < count_rigt) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks of the three points perform a right or a left or no turn by
	 * computing the scalar product.
	 * 
	 * @param a
	 *            The first point
	 * @param b
	 *            The second point
	 * @param c
	 *            The third point
	 * @return CW CCW or collinear
	 */
	private Turn get_Turn(Colored_point a, Colored_point b, Colored_point c) {
		long cross_product = (((long) b.x - a.x) * ((long) c.y - a.y))
				- (((long) b.y - a.y) * ((long) c.x - a.x));

		if (cross_product > 0) {
			return Turn.COUNTER_CLOCKWISE;
		} else if (cross_product < 0) {
			return Turn.CLOCKWISE;
		} else {
			return Turn.COLLINEAR;
		}
	}

	/**
	 * Checks if the input is fine to perform the polygon triangulation.
	 * 
	 * @param data
	 *            the data container with the input
	 * @return false if the input is fine else true
	 */
	private boolean bad_input(Draw_Container data) {
		if (data.get_polygons_self().size() == 0
				|| data.get_polygons().size() == 0
				|| data.get_polygons_self().size() > 1
				|| data.get_points().size() != data.get_polygons_self().get(0)
						.get_points().size()) {
			JOptionPane
					.showMessageDialog(
							null,
							"Can not compute polygon triangulation, wrong input values.",
							"Error", JOptionPane.ERROR_MESSAGE);
			System.err.println("No polygons.");
			return true;
		} else {
			return false;
		}
	}

}
