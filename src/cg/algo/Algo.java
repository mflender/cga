/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo;

import java.util.concurrent.TimeUnit;
import cg.Drawing_Space;
import cg.drawable.Draw_Container;

/**
 * A generic class for a single Computational Geometry algorithm.
 * 
 * @author malte
 * 
 */
public abstract class Algo {

	/**
	 * Computes a algorithm and stores the results as lines and points.
	 * 
	 * @param data
	 *            The data to Perform the algo
	 * @param draw_panel
	 *            the panel where the algo can draw while execution on
	 */
	public abstract void compute(Draw_Container data, Drawing_Space draw_panel);

	/**
	 * Pauses the algo a given time in ms an paints the current state.
	 * 
	 * @param draw_panel
	 *            the panel to paint on.
	 * @param time
	 *            the time in ms to wait
	 */
	protected void wait_paint(Drawing_Space draw_panel, int time) {
		try {
			TimeUnit.MILLISECONDS.sleep(time);
			draw_panel.paintImmediately(draw_panel.getX(), draw_panel.getY(),
					draw_panel.getWidth(), draw_panel.getHeight());
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

}
