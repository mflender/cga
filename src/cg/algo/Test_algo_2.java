/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo;

import java.awt.Color;

import cg.Drawing_Space;
import cg.drawable.Colored_line;
import cg.drawable.Draw_Container;

/**
 * A basic test algo.
 * 
 * @author malte
 * 
 */
public class Test_algo_2 extends Algo {

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {
		if (!data.get_points().isEmpty() && data.get_lines() != null) {
			for (int i = 0; i < data.get_points().size(); i++) {
				for (int j = 0; j < data.get_points().size(); j++) {

					data.get_points().get(i).set_color(new Color(255, 0, 0));

					data.get_lines().add(
							new Colored_line(data.get_points().get(i), data
									.get_points().get(j), new Color(0, 0, 0)));

					wait_paint(draw_panel, 0);
				}
			}
		} else {
			System.err.println("Algo_2 error: no Data");
		}

	}

}
