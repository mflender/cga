/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.kdtree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.collections15.Transformer;

import cg.Drawing_Space;
import edu.uci.ics.jung.algorithms.layout.RadialTreeLayout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

/**
 * Draws a KDtree visualization and paints the nodes in different colors.
 * 
 * @author malte
 * 
 */
public class KDtree_GUI {
	private Forest<KDNode, Integer> KDtree_vis = null;
	private Drawing_Space draw = null;
	private KDTree kdtree = null;
	private final int X_DIM_TREE = 500;
	private final int Y_DIM_TREE = 500;
	private final int X_DIM_FRAME = 500;
	private final int Y_DIM_FRAME = 500;
	private VisualizationViewer<KDNode, Integer> viewer = null;

	/**
	 * A new GUI with all necessary data.
	 * 
	 * @param draw_panel
	 *            the algo GUI to set the position to.
	 * @param kdtree
	 *            the kd tree object for callback functions
	 */
	public KDtree_GUI(Drawing_Space draw_panel, KDTree kdtree) {
		KDtree_vis = new DelegateForest<KDNode, Integer>();
		draw = draw_panel;
		this.kdtree = kdtree;
	}

	/**
	 * Draws the tree in a additional window.
	 */
	public void draw_Tree() {
		JFrame frame = new JFrame("KDTree");
		make_panel();
		frame.add(viewer);
		frame.add(make_Button(), BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocation(draw.getLocation().x,
				draw.getLocation().y + draw.getSize().width);
		frame.setVisible(true);
	}

	/**
	 * Makes the button to perform a range search in the given range.
	 * 
	 * @return the JPanel with the button
	 */
	private JPanel make_Button() {
		final JButton range = new JButton("Range Search");
		final JPanel button_panel = new JPanel();
		range.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				kdtree.range_search();
			}
		});

		button_panel.setBackground(Color.LIGHT_GRAY);
		button_panel.add(range);
		return button_panel;
	}

	/**
	 * Makes a new layout and sets all options so everything looks good
	 */
	private void make_panel() {
		TreeLayout<KDNode, Integer> layout = new TreeLayout<KDNode, Integer>(
				KDtree_vis, 80, 80);

		RadialTreeLayout<KDNode, Integer> radialLayout = new RadialTreeLayout<KDNode, Integer>(
				KDtree_vis);
		radialLayout.setSize(new Dimension(X_DIM_TREE, Y_DIM_TREE));

		viewer = new VisualizationViewer<KDNode, Integer>(layout);
		viewer.setPreferredSize(new Dimension(X_DIM_FRAME, Y_DIM_FRAME));
		viewer.getRenderContext().setVertexLabelTransformer(
				new ToStringLabeller<KDNode>());
		viewer.getRenderContext().setEdgeShapeTransformer(
				new EdgeShape.Line<KDNode, Integer>());

		DefaultModalGraphMouse<KDNode, Integer> gm = new DefaultModalGraphMouse<KDNode, Integer>();
		gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		viewer.setGraphMouse(gm);
		viewer.addKeyListener(gm.getModeKeyListener());

		viewer.getRenderer().getVertexLabelRenderer().setPosition(Position.S);

		paint_nodes(null);
	}

	/**
	 * Paints a node in a preset color depending on the nodes state.
	 * 
	 * @param node
	 *            the node to paint
	 */
	public void paint_nodes(final KDNode node) {
		Transformer<KDNode, Paint> vertexPaint = new Transformer<KDNode, Paint>() {
			@Override
			public Paint transform(KDNode node) {
				if (node.is_in_search_area()) {
					return new Color(255, 0, 0);
				} else if (node.is_visited()) {
					return new Color(0, 255, 0);
				} else {
					return null;
				}
			}
		};
		viewer.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
		viewer.repaint();
	}

	/**
	 * @return the KDtree to add edges and vertex.
	 */
	public Forest<KDNode, Integer> get_kdtree() {
		return KDtree_vis;
	}
}
