/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.kdtree;

import cg.drawable.Colored_line;
import cg.drawable.Colored_point;

/**
 * A node in the KD tree.
 * 
 * @author malte
 * 
 */
public class KDNode {
	private int x_min = 0;
	private int y_min = 0;
	private int x_max = 0;
	private int y_max = 0;
	private States state = null;
	private KDNode parent = null;
	private KDNode left_child = null;
	private KDNode right_child = null;
	private String name = "";
	private Colored_point point = null;
	private Colored_line line = null;
	private boolean visited = false;
	private boolean in_search_area = false;

	public static enum States {
		vertical, horizontal
	}

	/**
	 * Creates a new KDNode with all necessary data.
	 * 
	 * @param parent
	 *            the parent node
	 * @param name
	 *            the name of the node to be printed in the tree
	 * @param state
	 *            the state of the node
	 * @param x_min
	 *            the x start value of the node area
	 * @param y_min
	 *            the y start value of the node area
	 * @param x_max
	 *            the x end value of the node area
	 * @param y_maxthe
	 *            y end value of the node area
	 */
	public KDNode(KDNode parent, String name, States state, int x_min,
			int y_min, int x_max, int y_max) {
		this.parent = parent;
		this.name = name;
		this.state = state;
		this.x_min = x_min;
		this.y_min = y_min;
		this.x_max = x_max;
		this.y_max = y_max;
	}

	/**
	 * @return the x_min
	 */
	public int get_x_min() {
		return x_min;
	}

	/**
	 * @param x_min
	 *            the x_min to set
	 */
	public void set_x_min(int x_min) {
		this.x_min = x_min;
	}

	/**
	 * @return the y_min
	 */
	public int get_y_min() {
		return y_min;
	}

	/**
	 * @param y_min
	 *            the y_min to set
	 */
	public void set_y_min(int y_min) {
		this.y_min = y_min;
	}

	/**
	 * @return the x_max
	 */
	public int get_x_max() {
		return x_max;
	}

	/**
	 * @param x_max
	 *            the x_max to set
	 */
	public void set_x_max(int x_max) {
		this.x_max = x_max;
	}

	/**
	 * @return the y_max
	 */
	public int get_y_max() {
		return y_max;
	}

	/**
	 * @param y_max
	 *            the y_max to set
	 */
	public void set_y_max(int y_max) {
		this.y_max = y_max;
	}

	/**
	 * @return the state
	 */
	public States get_state() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void set_state(States state) {
		this.state = state;
	}

	/**
	 * @return the parent
	 */
	public KDNode get_parent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void set_parent(KDNode parent) {
		this.parent = parent;
	}

	/**
	 * @return the left_child
	 */
	public KDNode get_left_child() {
		return left_child;
	}

	/**
	 * @param left_child
	 *            the left_child to set
	 */
	public void set_left_child(KDNode left_child) {
		this.left_child = left_child;
	}

	/**
	 * @return the right_child
	 */
	public KDNode get_right_child() {
		return right_child;
	}

	/**
	 * @param right_child
	 *            the right_child to set
	 */
	public void set_right_child(KDNode right_child) {
		this.right_child = right_child;
	}

	@Override
	public String toString() {
		String out = "";
		if (parent != null) {
			if (parent.left_child.equals(this)) {
				out += "L ";
			} else if (parent.right_child.equals(this)) {
				out += "R ";
			}
		}
		if (point != null) {
			out += name + " " + point.x + "/" + point.y;
		} else {
			out += name;
		}
		return out;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void set_name(String name) {
		this.name = name;
	}

	/**
	 * @return the point
	 */
	public Colored_point get_point() {
		return point;
	}

	/**
	 * @param point
	 *            the point to set
	 */
	public void set_point(Colored_point point) {
		this.point = point;
	}

	/**
	 * @return the line
	 */
	public Colored_line get_line() {
		return line;
	}

	/**
	 * @param line
	 *            the line to set
	 */
	public void set_line(Colored_line line) {
		this.line = line;
	}

	/**
	 * Checks if the node is a leaf node.
	 * 
	 * @return true if the node is a leaf node else false
	 */
	public boolean is_Leaf() {
		if (left_child == null && right_child == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if the node is the root node.
	 * 
	 * @return true if the node is the root node else false
	 */
	public boolean is_root() {
		if (parent == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the visited
	 */
	public boolean is_visited() {
		return visited;
	}

	/**
	 * @param visited
	 *            the visited to set
	 */
	public void set_visited(boolean visited) {
		this.visited = visited;
	}

	/**
	 * @return the in_search_area
	 */
	public boolean is_in_search_area() {
		return in_search_area;
	}

	/**
	 * @param in_search_area
	 *            the in_search_area to set
	 */
	public void set_in_search_area(boolean in_search_area) {
		this.in_search_area = in_search_area;
	}
}
