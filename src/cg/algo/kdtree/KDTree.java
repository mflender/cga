/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.kdtree;

import java.awt.Color;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import cg.Drawing_Space;
import cg.algo.Algo;
import cg.algo.kdtree.KDNode.States;
import cg.drawable.Colored_line;
import cg.drawable.Colored_point;
import cg.drawable.Colored_polygon_self;
import cg.drawable.Draw_Container;

/**
 * A 2d tree and range search implementation.
 * 
 * @author malte
 * 
 */
public class KDTree extends Algo {
	private KDtree_GUI gui = null;
	private Drawing_Space panel = null;
	private Colored_polygon_self range = null;
	private KDNode root = null;

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {
		if (bad_input(data)) {
			return;
		}

		if (data.get_polygons_self().get(0) != null) {
			data.get_polygons_self().get(0).set_color(new Color(255, 0, 0));
			range = data.get_polygons_self().get(0);
		} else {
			return;
		}

		panel = draw_panel;
		data.get_lines().clear();
		data.get_polygons().clear();
		remove_double_points(data);

		gui = new KDtree_GUI(draw_panel, this);
		create_root(data, draw_panel);

		gui.draw_Tree();
	}

	/**
	 * Creates the root node and starts the recursion to create the whole tree.
	 * 
	 * @param data
	 *            the data to store the lines in
	 * @param draw_panel
	 *            the panel to get the bounds out
	 */
	private void create_root(Draw_Container data, Drawing_Space draw_panel) {
		root = new KDNode(null, "Root", States.horizontal, draw_panel.getX(),
				draw_panel.getY(), draw_panel.getWidth(),
				draw_panel.getHeight());

		List<Colored_point> y_sort = sort_points_y(data.get_points());
		List<Colored_point> x_sort = sort_points_x(data.get_points());
		List<Colored_point> x_1 = new ArrayList<>();
		List<Colored_point> x_2 = new ArrayList<>();

		int median_pos = y_sort.size() / 2;
		Colored_point point = y_sort.get(median_pos);
		root.set_point(point);

		List<Colored_point> y_1 = new ArrayList<>(y_sort.subList(0, median_pos));
		List<Colored_point> y_2 = new ArrayList<>(y_sort.subList(
				median_pos + 1, y_sort.size()));

		for (Colored_point point_x : x_sort) {
			if (y_1.contains(point_x)) {
				x_1.add(point_x);
			} else if (y_2.contains(point_x)) {
				x_2.add(point_x);
			}
		}

		Colored_line line = new Colored_line(root.get_x_min(), point.getY(),
				root.get_x_max(), point.getY(), new Color(0));
		data.get_lines().add(line);
		root.set_line(line);

		gui.get_kdtree().addVertex(root);

		root.set_left_child(create_tree(x_2, y_2, data, root, root.get_x_min(),
				(int) point.getY(), root.get_x_max(), root.get_y_max()));
		root.set_right_child(create_tree(x_1, y_1, data, root,
				root.get_x_min(), root.get_y_min(), root.get_x_max(),
				(int) point.getY()));
	}

	/**
	 * Creates the tree in recursive manner.
	 * 
	 * @param points
	 *            the points to insert
	 * @param data
	 *            the data to store the lines in
	 * @param parent
	 *            the parent node
	 * @param x_min
	 *            x value of the first region coordinate
	 * @param y_min
	 *            y value of the first region coordinate
	 * @param x_max
	 *            x value of the second region coordinate
	 * @param y_max
	 *            y value of the second region coordinate
	 * @return the subtree
	 */
	private KDNode create_tree(List<Colored_point> x_sort,
			List<Colored_point> y_sort, Draw_Container data, KDNode parent,
			int x_min, int y_min, int x_max, int y_max) {
		if (x_sort.size() > 0 && y_sort.size() > 0) {

			int median_pos = 0;
			KDNode child = null;
			Colored_line line = null;
			Colored_point point = null;

			if (parent.get_state() == States.vertical) {
				median_pos = y_sort.size() / 2;
				point = y_sort.get(median_pos);

				List<Colored_point> y_1 = new ArrayList<>(y_sort.subList(0,
						median_pos));
				List<Colored_point> y_2 = new ArrayList<>(y_sort.subList(
						median_pos + 1, y_sort.size()));
				List<Colored_point> x_1 = new ArrayList<>();
				List<Colored_point> x_2 = new ArrayList<>();

				for (Colored_point point_x : x_sort) {
					if (y_1.contains(point_x)) {
						x_1.add(point_x);
					} else if (y_2.contains(point_x)) {
						x_2.add(point_x);
					}
				}

				child = new KDNode(parent, "H", States.horizontal, x_min,
						y_min, x_max, y_max);
				line = new Colored_line(x_min, point.getY(), x_max,
						point.getY(), new Color(0));

				child.set_left_child(create_tree(x_1, y_1, data, child, x_min,
						y_min, x_max, (int) point.getY()));
				child.set_right_child(create_tree(x_2, y_2, data, child, x_min,
						(int) point.getY(), x_max, y_max));
			} else {
				median_pos = x_sort.size() / 2;
				point = x_sort.get(median_pos);

				List<Colored_point> x_1 = new ArrayList<>(x_sort.subList(0,
						median_pos));
				List<Colored_point> x_2 = new ArrayList<>(x_sort.subList(
						median_pos + 1, x_sort.size()));
				List<Colored_point> y_1 = new ArrayList<>();
				List<Colored_point> y_2 = new ArrayList<>();

				for (Colored_point point_y : y_sort) {
					if (x_1.contains(point_y)) {
						y_1.add(point_y);
					} else if (x_2.contains(point_y)) {
						y_2.add(point_y);
					}
				}

				child = new KDNode(parent, "V", States.vertical, x_min, y_min,
						x_max, y_max);
				line = new Colored_line(point.getX(), y_min, point.getX(),
						y_max, new Color(0));

				child.set_left_child(create_tree(x_1, y_1, data, child, x_min,
						y_min, (int) point.getX(), y_max));
				child.set_right_child(create_tree(x_2, y_2, data, child,
						(int) point.getX(), y_min, x_max, y_max));
			}

			gui.get_kdtree().addVertex(child);
			gui.get_kdtree().addEdge(child.hashCode(), parent, child);

			child.set_parent(parent);
			child.set_point(point);
			child.set_line(line);
			data.get_lines().add(line);

			return child;
		} else {
			return null;
		}
	}

	/**
	 * The recursive range search.
	 * 
	 * @param node
	 *            the actual node
	 * @param direction
	 *            the direction to search
	 * @param query_range
	 *            the range to query
	 */
	private void range_search(KDNode node, States direction,
			Colored_polygon_self query_range) {
		States d_new = null;
		int coord = 0;
		int l = 0;
		int r = 0;

		if (node != null) {
			node.set_visited(true);
			node.get_point().set_color(new Color(0, 255, 0));

			if (direction == States.horizontal) {
				l = query_range.get_points().get(1).y;
				r = query_range.get_points().get(3).y;
				coord = (int) node.get_point().getY();
				d_new = States.vertical;
			} else {
				l = query_range.get_points().get(0).x;
				r = query_range.get_points().get(1).x;
				coord = (int) node.get_point().getX();
				d_new = States.horizontal;
			}

			if (range_contains_point(node.get_point(), query_range)) {
				node.set_in_search_area(true);
				node.get_point().set_color(new Color(255, 0, 0));
				System.out.println(node);
			}
			if (l <= coord) { // add <= for closed search range
				range_search(node.get_left_child(), d_new, query_range);
			}
			if (coord <= r) { // add >= for closed search range
				range_search(node.get_right_child(), d_new, query_range);
			}
			gui.paint_nodes(node);
			panel.repaint();
		}
	}

	/**
	 * Checks if the range is a rectangle and performs a range search.
	 */
	public void range_search() {
		if (check_quad_space(range)) {
			range_search(root, States.horizontal, range);
		} else {
			System.err
					.println("KDTree.range_search: query range is not a rectangle.");
		}
	}

	/**
	 * Checks if the given query range is a rectangle with 4 points CCW sorted.
	 * 
	 * @param query_range
	 *            the query range to check
	 * @return true if the range is a rectangle else false
	 */
	private boolean check_quad_space(Colored_polygon_self query_range) {
		boolean quad = false;

		if (query_range.get_points().get(0).y == query_range.get_points()
				.get(1).y
				&& query_range.get_points().get(3).y == query_range
						.get_points().get(2).y
				&& query_range.get_points().get(0).x == query_range
						.get_points().get(3).x
				&& query_range.get_points().get(1).x == query_range
						.get_points().get(2).x) {
			quad = true;
		}
		return quad;
	}

	/**
	 * Checks if the point lies in the given rectangle.
	 * 
	 * @param point
	 *            the point to check
	 * @param query_range
	 *            the range to check
	 * @return true if the point is in the rectangle false if not
	 */
	private boolean range_contains_point(Colored_point point,
			Colored_polygon_self query_range) {
		boolean contains = false;
		// add >= for closed search range
		if (point.x >= query_range.get_points().get(0).x
				&& point.x <= query_range.get_points().get(1).x
				&& point.y >= query_range.get_points().get(0).y
				&& point.y <= query_range.get_points().get(3).y) {
			contains = true;
		}
		return contains;
	}

	/**
	 * Sorts all points in x direction.
	 * 
	 * @param points
	 *            the points to sort
	 * @return the sorted points
	 */
	private List<Colored_point> sort_points_x(List<Colored_point> points) {
		List<Colored_point> x_sort_tmp = new ArrayList<>();
		TreeSet<Colored_point> sort_set = new TreeSet<>(
				new Comparator<Colored_point>() {
					@Override
					public int compare(Colored_point a, Colored_point b) {
						if (a.equals(b) || a == b) {
							return 0;
						} else if (a.x > b.x) {
							return 1;
						} else {
							return -1;
						}
					}
				});
		sort_set.addAll(points);
		x_sort_tmp.addAll(sort_set);
		return x_sort_tmp;
	}

	/**
	 * Sorts all points in y direction.
	 * 
	 * @param points
	 *            the points to sort
	 * @return the sorted points
	 */
	private List<Colored_point> sort_points_y(List<Colored_point> points) {
		List<Colored_point> y_sort_tmp = new ArrayList<>();
		TreeSet<Colored_point> sort_set = new TreeSet<>(
				new Comparator<Colored_point>() {
					@Override
					public int compare(Colored_point a, Colored_point b) {
						if (a.equals(b) || a == b) {
							return 0;
						} else if (a.y > b.y) {
							return 1;
						} else {
							return -1;
						}
					}
				});
		sort_set.addAll(points);
		y_sort_tmp.addAll(sort_set);
		return y_sort_tmp;
	}

	/**
	 * Removes the points that are in the polygon out of the points.
	 * 
	 * @param data
	 *            the data container to remove the points
	 */
	private void remove_double_points(Draw_Container data) {
		List<Colored_point> remove_point = new ArrayList<>();
		for (Colored_point point : data.get_polygons_self().get(0).get_points()) {
			remove_point.add(point);
		}
		data.get_points().removeAll(remove_point);
	}

	/**
	 * Checks if the input is fine to perform the KDtree.
	 * 
	 * @param data
	 *            the data container with the input
	 * @return false if the input is fine else true
	 */
	private boolean bad_input(Draw_Container data) {
		if (data.get_polygons_self().size() == 0
				|| data.get_polygons().size() == 0
				|| data.get_points().size() == 0
				|| data.get_polygons().size() > 1) {
			JOptionPane.showMessageDialog(null,
					"Can not compute KDtree, wrong input values.", "Error",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("No polygons.");
			return true;
		} else {
			return false;
		}
	}

}
