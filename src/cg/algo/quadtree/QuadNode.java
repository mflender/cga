/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.quadtree;

import java.util.ArrayList;
import java.util.List;

import cg.drawable.Colored_line;
import cg.drawable.Colored_point;

public class QuadNode {
	private QuadNode NE = null;
	private QuadNode NW = null;
	private QuadNode SW = null;
	private QuadNode SE = null;
	private QuadNode parent = null;
	private String name = "";
	private int x_min = 0;
	private int y_min = 0;
	private int x_max = 0;
	private int y_max = 0;
	private Colored_line vertical_line = null;
	private Colored_line horizontal_line = null;
	private Colored_point replace_point = null;
	private states state = null;
	private List<Colored_point> points = new ArrayList<>();

	public static enum states {
		NW, NE, SW, SE, Root
	}

	/**
	 * Creates a new Node with its initial values.
	 * 
	 * @param parent
	 *            the parent of the node (root has null as parent)
	 * @param name
	 *            the name of the node
	 * @param state
	 *            the state of the node
	 */
	public QuadNode(QuadNode parent, String name, states state) {
		this.parent = parent;
		this.name = name;
		this.state = state;
	}

	/**
	 * Adds a new NE child.
	 * 
	 * @param NE
	 *            the NE child
	 */
	public void addNE(QuadNode NE) {
		this.set_NE(NE);
	}

	/**
	 * Adds a new NW child.
	 * 
	 * @param NW
	 *            the NW child
	 */
	public void addNW(QuadNode NW) {
		this.set_NW(NW);
	}

	/**
	 * Adds a new SW child.
	 * 
	 * @param SW
	 *            the SW child
	 */
	public void addSW(QuadNode SW) {
		this.set_SW(SW);
	}

	/**
	 * Adds a new SE child.
	 * 
	 * @param SE
	 *            the SE child
	 */
	public void addSE(QuadNode SE) {
		this.set_SE(SE);
	}

	/**
	 * @return the name of the root
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * @return the parent
	 */
	public QuadNode get_Parent() {
		return parent;
	}

	/**
	 * @return the x coordinate of the start point
	 */
	public int get_x_min() {
		return x_min;
	}

	/**
	 * @param x_min
	 *            sets the x coordinate of the start point
	 */
	public void set_x_min(int x_min) {
		this.x_min = x_min;
	}

	/**
	 * @return the y coordinate of the start point
	 */
	public int get_y_min() {
		return y_min;
	}

	/**
	 * @param y_min
	 *            sets the y coordinate of the start point
	 */
	public void set_y_min(int y_min) {
		this.y_min = y_min;
	}

	/**
	 * @return the x coordinate of the end point
	 */
	public int get_x_max() {
		return x_max;
	}

	/**
	 * @param x_max
	 *            sets the x coordinate of the end point
	 */
	public void set_x_max(int x_max) {
		this.x_max = x_max;
	}

	/**
	 * @return the y coordinate of the end point
	 */
	public int get_y_max() {
		return y_max;
	}

	/**
	 * @param y_max
	 *            sets the y coordinate of the end point
	 */
	public void set_y_max(int y_max) {
		this.y_max = y_max;
	}

	/**
	 * @param vertical_line
	 *            set the vertical line to split the node
	 */
	public void set_vertical_line(Colored_line vertical_line) {
		this.vertical_line = vertical_line;
	}

	/**
	 * @return the vertical_line
	 */
	public Colored_line get_vertical_line() {
		return vertical_line;
	}

	/**
	 * 
	 * @param horizontal_line
	 *            set the horizontal line to split the node
	 */
	public void set_horizontal_line(Colored_line horizontal_line) {
		this.horizontal_line = horizontal_line;
	}

	/**
	 * @return the horizontal_line
	 */
	public Colored_line get_horizontal_line() {
		return horizontal_line;
	}

	/**
	 * @return the nE
	 */
	public QuadNode get_NE() {
		return NE;
	}

	/**
	 * @param nE
	 *            the nE to set
	 */
	public void set_NE(QuadNode nE) {
		NE = nE;
	}

	/**
	 * @return the nW
	 */
	public QuadNode get_NW() {
		return NW;
	}

	/**
	 * @param nW
	 *            the nW to set
	 */
	public void set_NW(QuadNode nW) {
		NW = nW;
	}

	/**
	 * @return the sW
	 */
	public QuadNode get_SW() {
		return SW;
	}

	/**
	 * @param sW
	 *            the sW to set
	 */
	public void set_SW(QuadNode sW) {
		SW = sW;
	}

	/**
	 * @return the sE
	 */
	public QuadNode get_SE() {
		return SE;
	}

	/**
	 * @param sE
	 *            the sE to set
	 */
	public void set_SE(QuadNode sE) {
		SE = sE;
	}

	/**
	 * @return the state
	 */
	public states get_state() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void set_state(states state) {
		this.state = state;
	}

	/**
	 * @return true if the node is a leaf else false
	 */
	public boolean is_leaf() {
		if (this.NE == null && this.SE == null && this.SW == null
				&& this.NW == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the replace_point
	 */
	public Colored_point get_replace_point() {
		return replace_point;
	}

	/**
	 * @param replace_point
	 *            the replace_point to set
	 */
	public void set_replace_point(Colored_point replace_point) {
		this.replace_point = replace_point;
	}

	/**
	 * @return the points
	 */
	public List<Colored_point> get_points() {
		return points;
	}

	/**
	 * @param points
	 *            the points to set
	 */
	public void set_points(List<Colored_point> points) {
		this.points = points;
	}
}
