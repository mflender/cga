/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.quadtree;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import cg.Drawing_Space;
import cg.algo.Algo;
import cg.algo.quadtree.QuadNode.states;
import cg.drawable.Colored_line;
import cg.drawable.Colored_point;
import cg.drawable.Draw_Container;

/**
 * Creates and draws a quadtree in the following manner:
 * 
 * +–––––+–––––+ 
 * |     |     | 
 * | NW  |  NE | 
 * |     |     | 
 * +–––––+–––––+ 
 * |     |     |
 * |  SW |  SE |
 * |     |     |
 * +–––––+–––––+
 * 
 * @author malte
 * 
 */

public class QuadTree extends Algo {
	private QuadTree_GUI jung = null;
	private Drawing_Space panel = null;
	private Draw_Container data = null;
	private QuadNode selected_node = null;
	private QuadNode neighbor_node = null;

	public static enum direction {
		north, south, east, west
	}

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {
		this.jung = new QuadTree_GUI(draw_panel, this);
		this.panel = draw_panel;
		this.data = data;
		
		data.get_lines().clear();
		data.get_polygons().clear();
		data.get_polygons_self().clear();

		QuadNode root = new QuadNode(null, "Root", states.Root);

		root.set_x_min(draw_panel.getX());
		root.set_y_min(draw_panel.getY());
		root.set_x_max(draw_panel.getWidth());
		root.set_y_max(draw_panel.getHeight());
		root.set_points(data.get_points());

		jung.get_quadtree().addVertex(root);
		create_tree(root, root.get_x_min(), root.get_y_min(), root.get_x_max(),
				root.get_y_max());
		jung.draw_Tree();
	}

	/**
	 * Creates the quadtree in a recursive manner.
	 * 
	 * @param data
	 *            the data to get the points out and lines in
	 * @param x_min
	 *            x value of the first region coordinate
	 * @param y_min
	 *            y value of the first region coordinate
	 * @param x_max
	 *            x value of the second region coordinate
	 * @param y_max
	 *            y value of the second region coordinate
	 */
	private void create_tree(QuadNode actual, int x_min, int y_min, int x_max,
			int y_max) {
		if (actual.get_points().size() > 1) {

			Colored_line vertical_line = new Colored_line((x_max + x_min) / 2,
					y_min, (x_max + x_min) / 2, y_max, new Color(0));
			Colored_line horizontal_line = new Colored_line(x_min,
					(y_max + y_min) / 2, x_max, (y_max + y_min) / 2, new Color(
							0));

			data.get_lines().add(vertical_line);
			data.get_lines().add(horizontal_line);

			actual.set_vertical_line(vertical_line);
			actual.set_horizontal_line(horizontal_line);

			wait_paint(panel, 0);

			// NE
			QuadNode NE = new QuadNode(actual, "NE", states.NE);
			NE.set_x_min((x_max + x_min) / 2);
			NE.set_y_min((y_max + y_min) / 2);
			NE.set_x_max(x_max);
			NE.set_y_max(y_max);
			NE.set_points(get_points_in_area(actual, NE));
			actual.addNE(NE);
			jung.get_quadtree().addVertex(NE);
			jung.get_quadtree().addEdge(NE.hashCode(), NE.get_Parent(), NE);
			create_tree(NE, (x_max + x_min) / 2, (y_max + y_min) / 2, x_max,
					y_max);

			// NW
			QuadNode NW = new QuadNode(actual, "NW", states.NW);
			NW.set_x_min(x_min);
			NW.set_y_min((y_max + y_min) / 2);
			NW.set_x_max((x_max + x_min) / 2);
			NW.set_y_max(y_max);
			NW.set_points(get_points_in_area(actual, NW));
			actual.addNW(NW);
			jung.get_quadtree().addVertex(NW);
			jung.get_quadtree().addEdge(NW.hashCode(), NW.get_Parent(), NW);
			create_tree(NW, x_min, (y_max + y_min) / 2, (x_max + x_min) / 2,
					y_max);

			// SW
			QuadNode SW = new QuadNode(actual, "SW", states.SW);
			SW.set_x_min(x_min);
			SW.set_y_min(y_min);
			SW.set_x_max((x_max + x_min) / 2);
			SW.set_y_max((y_max + y_min) / 2);
			SW.set_points(get_points_in_area(actual, SW));
			actual.addSW(SW);
			jung.get_quadtree().addVertex(SW);
			jung.get_quadtree().addEdge(SW.hashCode(), SW.get_Parent(), SW);
			create_tree(SW, x_min, y_min, (x_max + x_min) / 2,
					(y_max + y_min) / 2);

			// SE
			QuadNode SE = new QuadNode(actual, "SE", states.SE);
			SE.set_x_min((x_max + x_min) / 2);
			SE.set_y_min(y_min);
			SE.set_x_max(x_max);
			SE.set_y_max((y_max + y_min) / 2);
			SE.set_points(get_points_in_area(actual, SE));
			actual.addSE(SE);
			jung.get_quadtree().addVertex(SE);
			jung.get_quadtree().addEdge(SE.hashCode(), SE.get_Parent(), SE);
			create_tree(SE, (x_max + x_min) / 2, y_min, x_max,
					(y_max + y_min) / 2);
		}
	}

	/**
	 * Creates a list with all points in the child node area from the list of
	 * the parent node
	 * 
	 * @param parent
	 *            the parent node to get the old list from
	 * @param child
	 *            the child node to get the area from
	 * @return the list with al points in the child node
	 */
	private List<Colored_point> get_points_in_area(QuadNode parent,
			QuadNode child) {
		List<Colored_point> area_points = new ArrayList<>();

		for (Colored_point point : parent.get_points()) {

			int x = (int) point.getX();
			int y = (int) point.getY();

			switch (child.get_state()) {
			case Root:
				if ((x >= child.get_x_min() && x <= child.get_x_max())
						&& (y >= child.get_y_min() && y <= child.get_y_max())) {
					area_points.add(point);
				}
				break;
			case NE:
				if ((x > child.get_x_min() && x <= child.get_x_max())
						&& (y > child.get_y_min() && y <= child.get_y_max())) {
					area_points.add(point);
				}
				break;
			case NW:
				if ((x >= child.get_x_min() && x <= child.get_x_max())
						&& (y > child.get_y_min() && y <= child.get_y_max())) {
					area_points.add(point);
				}
				break;
			case SW:
				if ((x >= child.get_x_min() && x <= child.get_x_max())
						&& (y >= child.get_y_min() && y <= child.get_y_max())) {
					area_points.add(point);
				}
				break;
			case SE:
				if ((x > child.get_x_min() && x <= child.get_x_max())
						&& (y >= child.get_y_min() && y <= child.get_y_max())) {
					area_points.add(point);
				}
				break;
			default:
				System.err.println("Quadtree.get_points_in_area: Error.");
				break;
			}
		}
		 
		return area_points;
	}

	/**
	 * Sets the selected node.
	 * 
	 * @param node
	 *            the node witch is selected
	 */
	public void select_Node(QuadNode node) {
		selected_node = node;
		color_node(selected_node, new Color(255, 0, 0));
	}

	/**
	 * Clears the node if the node is unselected.
	 * 
	 * @param node
	 *            the new unselected node
	 */
	public void unselect_Node(QuadNode node) {
		uncolor_node(selected_node);
		uncolor_node(neighbor_node);
		selected_node = null;
		neighbor_node = null;
		jung.paint_nodes(neighbor_node);
	}

	/**
	 * Colors a given node with a given color. if the node is empty a dot is
	 * shown in the node.
	 * 
	 * @param node
	 *            the node to be colored
	 * @param color
	 *            the color in witch the node is drawn
	 */
	private void color_node(QuadNode node, Color color) {
		if (node != null) {
			Colored_line horizontal = node.get_horizontal_line();
			Colored_line vertical = node.get_vertical_line();
			if (horizontal != null && vertical != null
					&& node.get_points().size() > 1) {
				horizontal.set_color(color);
				vertical.set_color(color);
			} else {
				Colored_point point = null;
				if (node.get_points().size() == 1) {
					point = node.get_points().get(0);
					point.set_color(color);
				} else {
					point = new Colored_point(
							(node.get_x_max() + node.get_x_min()) / 2,
							(node.get_y_max() + node.get_y_min()) / 2, color);
					data.get_points().add(point);
					node.set_replace_point(point);
				}
			}
		}
		wait_paint(panel, 0);
	}

	/**
	 * Removes the color from a given node.
	 * 
	 * @param node
	 *            the node to be cleared
	 */
	private void uncolor_node(QuadNode node) {
		if (node != null) {
			Colored_line horizontal = node.get_horizontal_line();
			Colored_line vertical = node.get_vertical_line();
			if (horizontal != null && vertical != null
					&& node.get_points().size() > 1) {
				horizontal.set_color(new Color(0));
				vertical.set_color(new Color(0));
			} else {
				Colored_point point = null;
				if (node.get_points().size() == 1) {
					point = node.get_points().get(0);
					point.set_color(new Color(0));
				} else {
					point = node.get_replace_point();
					node.set_replace_point(null);
					data.get_points().remove(point);
				}
			}
		}
		wait_paint(panel, 0);
	}

	/**
	 * Sets and colors a new neighbor_node according to the key input direction.
	 * 
	 * @param direct
	 *            the direction
	 */
	public void get_neighbor(direction direct) {
		if (neighbor_node != null) {
			uncolor_node(neighbor_node);
		}
		if (selected_node != null) {
			switch (direct) {
			case north:
				neighbor_node = get_north_neighbor(selected_node);
				break;
			case south:
				neighbor_node = get_south_neighbor(selected_node);
				break;
			case east:
				neighbor_node = get_east_neighbor(selected_node);
				break;
			case west:
				neighbor_node = get_west_neighbor(selected_node);
				break;
			default:
				System.err.println("Quadtree.get_neighbor: no direction.");
				break;
			}
			color_node(neighbor_node, new Color(0, 255, 0));
		}
		jung.paint_nodes(neighbor_node);
	}

	/**
	 * Finds the west neighbor of a given node.
	 * 
	 * @param node
	 *            the given node
	 * @return the neighbor or if there is no neighbor null
	 */
	private QuadNode get_west_neighbor(QuadNode node) {
		if (node.get_state() == states.Root) {
			return null;
		} else if (node.get_state() == states.NE) {
			return node.get_Parent().get_NW();
		} else if (node.get_state() == states.SE) {
			return node.get_Parent().get_SW();
		}
		QuadNode neighbor = get_west_neighbor(node.get_Parent());
		if (neighbor == null || neighbor.is_leaf()) {
			return neighbor;
		} else {
			if (node.get_state() == states.NW) {
				return neighbor.get_NE();
			} else {
				return neighbor.get_SE();
			}
		}
	}

	/**
	 * Finds the east neighbor of a given node.
	 * 
	 * @param node
	 *            the given node
	 * @return the neighbor or if there is no neighbor null
	 */
	private QuadNode get_east_neighbor(QuadNode node) {
		if (node.get_state() == states.Root) {
			return null;
		} else if (node.get_state() == states.NW) {
			return node.get_Parent().get_NE();
		} else if (node.get_state() == states.SW) {
			return node.get_Parent().get_SE();
		}
		QuadNode neighbor = get_east_neighbor(node.get_Parent());
		if (neighbor == null || neighbor.is_leaf()) {
			return neighbor;
		} else {
			if (node.get_state() == states.NE) {
				return neighbor.get_NW();
			} else {
				return neighbor.get_SW();
			}
		}
	}

	/**
	 * Finds the south neighbor of a given node.
	 * 
	 * @param node
	 *            the given node
	 * @return the neighbor or if there is no neighbor null
	 */
	private QuadNode get_south_neighbor(QuadNode node) {
		if (node.get_state() == states.Root) {
			return null;
		} else if (node.get_state() == states.NW) {
			return node.get_Parent().get_SW();
		} else if (node.get_state() == states.NE) {
			return node.get_Parent().get_SE();
		}
		QuadNode neighbor = get_south_neighbor(node.get_Parent());
		if (neighbor == null || neighbor.is_leaf()) {
			return neighbor;
		} else {
			if (node.get_state() == states.SW) {
				return neighbor.get_NW();
			} else {
				return neighbor.get_NE();
			}
		}
	}

	/**
	 * Finds the north neighbor of a given node.
	 * 
	 * @param node
	 *            the given node
	 * @return the neighbor or if there is no neighbor null
	 */
	private QuadNode get_north_neighbor(QuadNode node) {
		if (node.get_state() == states.Root) {
			return null;
		} else if (node.get_state() == states.SW) {
			return node.get_Parent().get_NW();
		} else if (node.get_state() == states.SE) {
			return node.get_Parent().get_NE();
		}
		QuadNode neighbor = get_north_neighbor(node.get_Parent());
		if (neighbor == null || neighbor.is_leaf()) {
			return neighbor;
		} else {
			if (node.get_state() == states.NW) {
				return neighbor.get_SW();
			} else {
				return neighbor.get_SE();
			}
		}
	}

}
