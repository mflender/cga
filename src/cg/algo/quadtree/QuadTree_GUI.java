/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.quadtree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.Paint;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.collections15.Transformer;

import cg.Drawing_Space;
import cg.algo.quadtree.QuadTree.direction;
import edu.uci.ics.jung.algorithms.layout.RadialTreeLayout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.picking.PickedState;

/**
 * Handles the JUNG2 api interface so a tree can be drawn in a additional
 * window.
 * 
 * @author malte
 * 
 */
public class QuadTree_GUI {
	private Forest<QuadNode, Integer> quadtree = null;
	private Drawing_Space draw = null;
	private QuadTree tree_split = null;
	private final int X_DIM_TREE = 500;
	private final int Y_DIM_TREE = 500;
	private final int X_DIM_FRAME = 500;
	private final int Y_DIM_FRAME = 500;
	private VisualizationViewer<QuadNode, Integer> viewer = null;

	/**
	 * Creates a new and empty JUNG tree
	 * 
	 * @param draw_panel
	 * @param quadtree_splitt
	 */
	public QuadTree_GUI(Drawing_Space draw_panel, QuadTree quadtree_splitt) {
		quadtree = new DelegateForest<QuadNode, Integer>();
		draw = draw_panel;
		tree_split = quadtree_splitt;
	}

	/**
	 * Prints a window with a graphical representation of the tree and a
	 * direction selection.
	 * 
	 * @param tree
	 */
	public void draw_Tree() {
		JFrame frame = new JFrame("QuadTree");
		make_panel();
		frame.add(viewer);
		frame.add(make_Buttons(), BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocationRelativeTo(draw);
		frame.setVisible(true);
	}

	/**
	 * Creates the buttons and action handler to traverse through the tree.
	 * 
	 * @return a JPanel that contains the buttons
	 */
	private JPanel make_Buttons() {
		final JButton north_button = new JButton("North");
		final JButton south_button = new JButton("South");
		final JButton east_button = new JButton("East");
		final JButton west_button = new JButton("West");
		final JPanel button_panel = new JPanel();

		north_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tree_split.get_neighbor(direction.north);
			}
		});

		south_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tree_split.get_neighbor(direction.south);
			}
		});

		east_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tree_split.get_neighbor(direction.east);
			}
		});

		west_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tree_split.get_neighbor(direction.west);
			}
		});

		button_panel.setBackground(Color.LIGHT_GRAY);
		button_panel.add(north_button);
		button_panel.add(south_button);
		button_panel.add(east_button);
		button_panel.add(west_button);

		return button_panel;
	}

	/**
	 * Creates a visualization from the given tree.
	 * 
	 * @param tree
	 *            the tree to visualize
	 * @return the VisualizationViewer of the tree
	 */
	private void make_panel() {
		TreeLayout<QuadNode, Integer> layout = new TreeLayout<QuadNode, Integer>(
				quadtree);

		RadialTreeLayout<QuadNode, Integer> radialLayout = new RadialTreeLayout<QuadNode, Integer>(
				quadtree);
		radialLayout.setSize(new Dimension(X_DIM_TREE, Y_DIM_TREE));

		viewer = new VisualizationViewer<QuadNode, Integer>(layout);
		viewer.setPreferredSize(new Dimension(X_DIM_FRAME, Y_DIM_FRAME));
		viewer.getRenderContext().setVertexLabelTransformer(
				new ToStringLabeller<QuadNode>());
		viewer.getRenderContext().setEdgeShapeTransformer(
				new EdgeShape.Line<QuadNode, Integer>());

		DefaultModalGraphMouse<QuadNode, Integer> gm = new DefaultModalGraphMouse<QuadNode, Integer>();
		gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		viewer.setGraphMouse(gm);
		viewer.addKeyListener(gm.getModeKeyListener());

		paint_nodes(null);
		add_node_picker();
	}

	/**
	 * Paints the nodes in the tree in color.
	 * 
	 * @param neighbor_node
	 *            the actual selected node
	 */
	public void paint_nodes(final QuadNode neighbor_node) {
		final PickedState<QuadNode> pickedState = viewer.getPickedVertexState();
		Transformer<QuadNode, Paint> vertexPaint = new Transformer<QuadNode, Paint>() {
			@Override
			public Paint transform(QuadNode node) {
				if (node == neighbor_node) {
					return new Color(0, 255, 0);
				} else if (pickedState.isPicked(node)) {
					return new Color(255, 0, 0);
				} else {
					return null;
				}
			}
		};
		viewer.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
		viewer.repaint();
	}

	/**
	 * Creates a new listener so picking a node calls a function.
	 * 
	 * @param viewer
	 *            the Viewer in witch the nodes shuld be picked
	 */
	private void add_node_picker() {
		final PickedState<QuadNode> pickedState = viewer.getPickedVertexState();
		pickedState.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				Object subject = e.getItem();
				if (subject instanceof QuadNode) {
					QuadNode vertex = (QuadNode) subject;
					if (pickedState.isPicked(vertex)) {
						tree_split.select_Node(vertex);
					} else {
						tree_split.unselect_Node(vertex);
					}
				}
			}
		});
	}

	/**
	 * @return the quadtree
	 */
	public Forest<QuadNode, Integer> get_quadtree() {
		return quadtree;
	}

}