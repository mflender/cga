/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
package cg.algo.scanline;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import cg.Drawing_Space;
import cg.algo.Algo;
import cg.drawable.Colored_line;
import cg.drawable.Colored_point;
import cg.drawable.Colored_polygon_self;
import cg.drawable.Draw_Container;

/**
 * A scan line algo to find the intersection polygon.
 * 
 * @author malte
 * 
 */
public class Scanline_Convex_hull extends Algo {
	private List<Colored_point> queue = new ArrayList<>();
	private List<Colored_line> scanline = new ArrayList<>(4);
	private Colored_polygon_self intersection_poly = new Colored_polygon_self();

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {
		if (data.get_polygons_self().size() == 0) {
			JOptionPane.showMessageDialog(null,
					"Can not compute Scan line, no polygons.", "Error",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("No polygons.");
			return;
		}

		for (int i = 0; i < 4; i++) {
			scanline.add(i, null);
		}

		data.get_points().clear();
		data.get_lines().clear();
		data.get_polygons().clear();

		data_to_queue(data);
		move_scanline(data);
		data.get_polygons_self().add(intersection_poly);
	}

	/**
	 * Orders the points of the polygons to the queue.
	 * 
	 * @param data
	 *            the sorce of the polygons
	 */
	private void data_to_queue(Draw_Container data) {
		Colored_polygon_self poly_a = data.get_polygons_self().get(0);
		Colored_polygon_self poly_b = data.get_polygons_self().get(1);

		Circular_double_linked_list<Colored_point> poly_list_a = new Circular_double_linked_list<>();
		Circular_double_linked_list<Colored_point> poly_list_b = new Circular_double_linked_list<>();

		poly_list_a.addAll(poly_a.get_points());
		poly_list_b.addAll(poly_b.get_points());

		Colored_point start_a = get_greatest_point(poly_list_a);
		Colored_point start_b = get_greatest_point(poly_list_b);

		while ((poly_list_a.size() + poly_list_b.size()) > 0) {
			Colored_point biggest = get_greater(start_a, start_b);

			if (!queue.contains(biggest)) {
				queue.add(biggest);
			}

			if (poly_list_a.size() == 0) {
				start_a = start_b;
			}
			if (poly_list_b.size() == 0) {
				start_b = start_a;
			}

			try {
				if (poly_list_a.contains(biggest)) {
					poly_list_a.set_actual(biggest);
					Colored_point before_a = poly_list_a.get_predecessor();
					Colored_point next_a = poly_list_a.get_successor();
					start_a = get_greater(before_a, next_a);
					poly_list_a.remove(biggest);
				} else {
					poly_list_b.set_actual(biggest);
					Colored_point before_b = poly_list_b.get_predecessor();
					Colored_point next_b = poly_list_b.get_successor();
					start_b = get_greater(before_b, next_b);
					poly_list_b.remove(biggest);
				}
			} catch (NullPointerException e) {
			}
		}
	}

	/**
	 * Moves the scan line from event point to event point and checks for
	 * intersections.
	 * 
	 * @param data
	 *            the data do write the intersection points in
	 */
	private void move_scanline(Draw_Container data) {
		Colored_polygon_self poly_a = data.get_polygons_self().get(0);
		Colored_polygon_self poly_b = data.get_polygons_self().get(1);

		Circular_double_linked_list<Colored_point> poly_list_a = new Circular_double_linked_list<>();
		Circular_double_linked_list<Colored_point> poly_list_b = new Circular_double_linked_list<>();

		poly_list_a.addAll(poly_a.get_points());
		poly_list_b.addAll(poly_b.get_points());

		boolean checked_0 = false;
		boolean checked_1 = false;
		boolean checked_2 = false;
		boolean checked_3 = false;

		for (Colored_point point : queue) {
			if (poly_a.get_points().contains(point)) {
				Colored_point next_a = poly_list_a.get_successor(point);
				Colored_point before_a = poly_list_a.get_predecessor(point);

				Colored_line line_a_n = new Colored_line(point, next_a, new Color(0, 0, 0));
				Colored_line line_a_b = new Colored_line(point, before_a, new Color(0, 0, 0));

				if ((scanline.get(0) == null) || !is_above_point(point, line_a_n)) {
					scanline.set(0, line_a_n);
					checked_0 = false;
					checked_1 = false;
				}

				if ((scanline.get(1) == null) || !is_above_point(point, line_a_b)) {
					scanline.set(1, line_a_b);
					checked_2 = false;
					checked_3 = false;
				}

			} else {
				Colored_point next_b = poly_list_b.get_successor(point);
				Colored_point before_b = poly_list_b.get_predecessor(point);

				Colored_line line_b_n = new Colored_line(point, next_b, new Color(0, 0, 0));
				Colored_line line_b_b = new Colored_line(point, before_b, new Color(0, 0, 0));

				if ((scanline.get(2) == null) || !is_above_point(point, line_b_n)) {
					scanline.set(2, line_b_n);
					checked_0 = false;
					checked_2 = false;
				}

				if ((scanline.get(3) == null) || !is_above_point(point, line_b_b)) {
					scanline.set(3, line_b_b);
					checked_1 = false;
					checked_3 = false;
				}
			}

			if (!scanline.contains(null)) {
				if (!checked_0) {
					test_intersections(scanline.get(0), scanline.get(2));
					checked_0 = true;
				}
				if (!checked_1) {
					test_intersections(scanline.get(0), scanline.get(3));
					checked_1 = true;
				}
				if (!checked_2) {
					test_intersections(scanline.get(1), scanline.get(2));
					checked_2 = true;
				}
				if (!checked_3) {
					test_intersections(scanline.get(1), scanline.get(3));
					checked_3 = true;
				}
			}
		}
	}

	private void test_intersections(Colored_line line_a, Colored_line line_b) {
		Colored_point intersection = compute_intersection_point(line_a, line_b);
		if (intersection != null) {
			intersection_poly.get_points().add(intersection);
		}
	}

	/**
	 * Returns true if the line is above the point.
	 * 
	 * @param point
	 *            the point to check the line position
	 * @param line
	 *            the line witch position will me checked
	 * @return true if the line is above or at the same height, false if the
	 *         line is under
	 */
	private boolean is_above_point(Colored_point point, Colored_line line) {
		boolean above = false;
		if (line.getP1().getY() >= point.getY()
				&& line.getP2().getY() >= point.getY()) {
			above = true;
		}
		return above;
	}

	/**
	 * Returns the lexicographically greatest point of the set.
	 * 
	 * @param poylgon
	 *            the set of points to check
	 * @return the greatest point
	 */
	private Colored_point get_greatest_point(
			Circular_double_linked_list<Colored_point> poylgon) {
		Colored_point greatest = poylgon.get(0);
		for (int i = 1; i < poylgon.size(); i++) {
			Colored_point tmp = poylgon.get(i);
			greatest = get_greater(tmp, greatest);
		}
		return greatest;
	}

	/**
	 * Returns the lexicographically greatest point of two points.
	 * 
	 * @param a
	 *            the first point to check
	 * @param b
	 *            the second point to check
	 * @return the lexicographically greatest point
	 */
	private Colored_point get_greater(Colored_point a, Colored_point b) {
		if (a.equals(b) || a == b) {
			return a;
		} else if ((a.y > b.y) || (a.y == b.y && a.x > b.x)) {
			return a;
		} else {
			return b;
		}
	}

	/**
	 * Computes the intersection between two lines an returns null if there is
	 * no intersection or a Colored point with the position of the intersection
	 * 
	 * @param line_a
	 *            the first line to test
	 * @param line_b
	 *            the second line to test
	 * @return null if no intersection else a point with the intersection
	 *         coordinates
	 */
	private Colored_point compute_intersection_point(Colored_line line_a,
			Colored_line line_b) {

		if (!line_a.intersectsLine(line_b)) {
			return null;
		}

		double px = line_a.getX1(), py = line_a.getY1(), rx = line_a.getX2()
				- px, ry = line_a.getY2() - py;
		double qx = line_b.getX1(), qy = line_b.getY1(), sx = line_b.getX2()
				- qx, sy = line_b.getY2() - qy;

		double det = sx * ry - sy * rx;
		if (det == 0) {
			return null;
		} else {
			double z = (sx * (qy - py) + sy * (px - qx)) / det;
			if (z == 0 || z == 1) {
				return null;
			}
			return new Colored_point((int) (px + z * rx), (int) (py + z * ry),
					new Color(255, 0, 0));
		}
	}

}
