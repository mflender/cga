/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.scanline;

import java.awt.Color;

import javax.swing.JOptionPane;

import cg.Drawing_Space;
import cg.algo.Algo;
import cg.drawable.Colored_line;
import cg.drawable.Colored_point;
import cg.drawable.Draw_Container;

/**
 * Computes all possible intersections between two polygons.
 * 
 * @author malte
 *
 */
public class Scanline_simple extends Algo {

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {
		if (data.get_polygons_self().size() == 0) {
			JOptionPane.showMessageDialog(null,
					"Can not compute Scan line, no polygons.", "Error",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("No polygons.");
			return;
		}

		data.get_lines().clear();
		data.get_points().clear();
		data.get_polygons().clear();

		for (Colored_line line_a : data.get_polygons_self().get(0).get_lines()) {
			for (Colored_line line_b : data.get_polygons_self().get(1)
					.get_lines()) {
				Colored_point intersection = compute_intersection_point(line_a,
						line_b);
				if (intersection != null) {
					data.get_points().add(intersection);
				}
			}
		}
	}

	/**
	 * Computes the intersection between two lines an returns null if there is
	 * no intersection or a Colored point with the position of the intersection
	 * 
	 * @param line_a
	 *            the first line to test
	 * @param line_b
	 *            the second line to test
	 * @return null if no intersection else a point with the intersection
	 *         coordinates
	 */
	private Colored_point compute_intersection_point(Colored_line line_a,
			Colored_line line_b) {

		if (!line_a.intersectsLine(line_b)) {
			return null;
		}

		double px = line_a.getX1(), py = line_a.getY1(), rx = line_a.getX2()
				- px, ry = line_a.getY2() - py;
		double qx = line_b.getX1(), qy = line_b.getY1(), sx = line_b.getX2()
				- qx, sy = line_b.getY2() - qy;

		double det = sx * ry - sy * rx;
		if (det == 0) {
			return null;
		} else {
			double z = (sx * (qy - py) + sy * (px - qx)) / det;
			if (z == 0 || z == 1) {
				return null;
			}
			return new Colored_point((int) (px + z * rx), (int) (py + z * ry),
					new Color(255, 0, 0));
		}
	}

}