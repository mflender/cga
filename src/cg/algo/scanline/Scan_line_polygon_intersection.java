/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.scanline;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import cg.Drawing_Space;
import cg.algo.Algo;
import cg.drawable.Colored_line;
import cg.drawable.Colored_point;
import cg.drawable.Colored_polygon_self;
import cg.drawable.Draw_Container;

/**
 * A scan line algo to find the intersection of two convex polygons.
 * 
 * @author malte
 * 
 */
public class Scan_line_polygon_intersection extends Algo {
	List<Colored_point> queue = new ArrayList<>();
	List<Colored_line> scanline = new ArrayList<>();

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {

		if (data.get_polygons_self().size() == 0) {
			JOptionPane.showMessageDialog(null,
					"Can not compute Scan line, no polygons.", "Error",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("No polygons.");
			return;
		}

		data.get_points().clear();
		data.get_lines().clear();
		data.get_polygons().clear();

		data_to_queue(data);
		move_scanline(data);
	}

	/**
	 * Orders the points of the polygons to the queue.
	 * 
	 * @param data
	 *            the sorce of the polygons
	 */
	private void data_to_queue(Draw_Container data) {
		Colored_polygon_self poly_a = data.get_polygons_self().get(0);
		Colored_polygon_self poly_b = data.get_polygons_self().get(1);

		Circular_double_linked_list<Colored_point> poly_list_a = new Circular_double_linked_list<>();
		Circular_double_linked_list<Colored_point> poly_list_b = new Circular_double_linked_list<>();

		poly_list_a.addAll(poly_a.get_points());
		poly_list_b.addAll(poly_b.get_points());

		Colored_point start_a = get_greatest_point(poly_list_a);
		Colored_point start_b = get_greatest_point(poly_list_b);

		while ((poly_list_a.size() + poly_list_b.size()) > 0) {
			Colored_point biggest = get_greater(start_a, start_b);

			if (!queue.contains(biggest)) {
				queue.add(biggest);
			}

			if (poly_list_a.size() == 0) {
				start_a = start_b;
			}
			if (poly_list_b.size() == 0) {
				start_b = start_a;
			}

			try {
				if (poly_list_a.contains(biggest)) {
					poly_list_a.set_actual(biggest);
					Colored_point before_a = poly_list_a.get_predecessor();
					Colored_point next_a = poly_list_a.get_successor();
					start_a = get_greater(before_a, next_a);
					poly_list_a.remove(biggest);
				} else {
					poly_list_b.set_actual(biggest);
					Colored_point before_b = poly_list_b.get_predecessor();
					Colored_point next_b = poly_list_b.get_successor();
					start_b = get_greater(before_b, next_b);
					poly_list_b.remove(biggest);
				}
			} catch (NullPointerException e) {
			}
		}
	}

	/**
	 * Moves the scan line from event point to event point and checks for
	 * intersections.
	 * 
	 * @param data
	 *            the data do write the intersection points in
	 */
	private void move_scanline(Draw_Container data) {

		Colored_polygon_self poly_a = data.get_polygons_self().get(0);
		Colored_polygon_self poly_b = data.get_polygons_self().get(1);

		Circular_double_linked_list<Colored_point> poly_list_a = new Circular_double_linked_list<>();
		Circular_double_linked_list<Colored_point> poly_list_b = new Circular_double_linked_list<>();

		poly_list_a.addAll(poly_a.get_points());
		poly_list_b.addAll(poly_b.get_points());

		Colored_point start_a = null;
		Colored_point start_b = null;

		while (queue.size() > 0) {
			
			if (poly_a.get_points().contains(queue.get(0))) {
				start_a = queue.get(0);
				queue.remove(start_a);
				try {
					scanline.remove(2);
					scanline.remove(3);
				} catch (IndexOutOfBoundsException e) {
				}
				scanline.add(2,
						new Colored_line(poly_list_a.get_predecessor(start_a),
								start_a, new Color(0, 0, 0)));
				scanline.add(
						3,
						new Colored_line(start_a, poly_list_a
								.get_successor(start_a), new Color(0, 0, 0)));
			} else if (poly_b.get_points().contains(queue.get(0))) {
				start_b = queue.get(0);
				queue.remove(start_b);
				try {
					scanline.remove(0);
					scanline.remove(1);
				} catch (IndexOutOfBoundsException e) {
				}
				scanline.add(0,
						new Colored_line(poly_list_b.get_predecessor(start_b),
								start_b, new Color(0, 0, 0)));
				scanline.add(
						1,
						new Colored_line(start_b, poly_list_b
								.get_successor(start_b), new Color(0, 0, 0)));
			}

			if (scanline.size() >= 4) {
				Colored_point a = compute_intersection_point(scanline.get(0),
						scanline.get(2));
				if (a != null) {
					data.get_points().add(a);
				}
				Colored_point b = compute_intersection_point(scanline.get(0),
						scanline.get(3));
				if (b != null) {
					data.get_points().add(b);
				}
				Colored_point c = compute_intersection_point(scanline.get(1),
						scanline.get(2));
				if (c != null) {
					data.get_points().add(c);
				}
				Colored_point d = compute_intersection_point(scanline.get(1),
						scanline.get(3));
				if (d != null) {
					data.get_points().add(d);
				}
			}
		}
	}

	/**
	 * Returns the lexicographically greatest point of the set.
	 * 
	 * @param poylgon
	 *            the set of points to check
	 * @return the greatest point
	 */
	private Colored_point get_greatest_point(
			Circular_double_linked_list<Colored_point> poylgon) {
		Colored_point greatest = poylgon.get(0);
		for (int i = 1; i < poylgon.size(); i++) {
			Colored_point tmp = poylgon.get(i);
			greatest = get_greater(tmp, greatest);
		}
		return greatest;
	}

	/**
	 * Returns the lexicographically greatest point of two points.
	 * 
	 * @param a
	 *            the first point to check
	 * @param b
	 *            the second point to check
	 * @return the lexicographically greatest point
	 */
	private Colored_point get_greater(Colored_point a, Colored_point b) {
		if (a.equals(b) || a == b) {
			return a;
		} else if ((a.y > b.y) || (a.y == b.y && a.x > b.x)) {
			return a;
		} else {
			return b;
		}
	}

	/**
	 * Computes the intersection between two lines an returns null if there is
	 * no intersection or a Colored point with the position of the intersection
	 * 
	 * @param line_a
	 *            the first line to test
	 * @param line_b
	 *            the second line to test
	 * @return null if no intersection else a point with the intersection
	 *         coordinates
	 */
	private Colored_point compute_intersection_point(Colored_line line_a,
			Colored_line line_b) {

		if (!line_a.intersectsLine(line_b)) {
			return null;
		}

		double px = line_a.getX1(), py = line_a.getY1(), rx = line_a.getX2()
				- px, ry = line_a.getY2() - py;
		double qx = line_b.getX1(), qy = line_b.getY1(), sx = line_b.getX2()
				- qx, sy = line_b.getY2() - qy;

		double det = sx * ry - sy * rx;
		if (det == 0) {
			return null;
		} else {
			double z = (sx * (qy - py) + sy * (px - qx)) / det;
			if (z == 0 || z == 1) {
				return null;
			}
			return new Colored_point((int) (px + z * rx), (int) (py + z * ry),
					new Color(255, 0, 0));
		}
	}

}