/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.scanline;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A double linked list where both ends are connected. So you can iterate in
 * both ways over the data.
 * 
 * @author malte
 * 
 * @param <E>
 *            the data to be stored in
 */
public class Circular_double_linked_list<E> implements Iterable<E> {
	private List<E> linkedList = new LinkedList<>();
	private E actual = null;

	/**
	 * Appends the specified element to the end of this list (optional
	 * operation).
	 * 
	 * <p>
	 * Lists that support this operation may place limitations on what elements
	 * may be added to this list. In particular, some lists will refuse to add
	 * null elements, and others will impose restrictions on the type of
	 * elements that may be added. List classes should clearly specify in their
	 * documentation any restrictions on what elements may be added.
	 * 
	 * @param e
	 *            element to be appended to this list
	 * @return <tt>true</tt> (as specified by {@link Collection#add})
	 * @throws UnsupportedOperationException
	 *             if the <tt>add</tt> operation is not supported by this list
	 * @throws ClassCastException
	 *             if the class of the specified element prevents it from being
	 *             added to this list
	 * @throws NullPointerException
	 *             if the specified element is null and this list does not
	 *             permit null elements
	 * @throws IllegalArgumentException
	 *             if some property of this element prevents it from being added
	 *             to this list
	 */
	public boolean add(E item) {
		return linkedList.add(item);
	}

	/**
	 * Returns <tt>true</tt> if this list contains the specified element. More
	 * formally, returns <tt>true</tt> if and only if this list contains at
	 * least one element <tt>e</tt> such that
	 * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
	 * 
	 * @param o
	 *            element whose presence in this list is to be tested
	 * @return <tt>true</tt> if this list contains the specified element
	 * @throws ClassCastException
	 *             if the type of the specified element is incompatible with
	 *             this list (<a
	 *             href="Collection.html#optional-restrictions">optional</a>)
	 * @throws NullPointerException
	 *             if the specified element is null and this list does not
	 *             permit null elements (<a
	 *             href="Collection.html#optional-restrictions">optional</a>)
	 */
	public boolean contains(E item) {
		return linkedList.contains(item);
	}

	/**
	 * Appends all of the elements in the specified collection to the end of
	 * this list, in the order that they are returned by the specified
	 * collection's iterator (optional operation). The behavior of this
	 * operation is undefined if the specified collection is modified while the
	 * operation is in progress. (Note that this will occur if the specified
	 * collection is this list, and it's nonempty.)
	 * 
	 * @param c
	 *            collection containing elements to be added to this list
	 * @return <tt>true</tt> if this list changed as a result of the call
	 * @throws UnsupportedOperationException
	 *             if the <tt>addAll</tt> operation is not supported by this
	 *             list
	 * @throws ClassCastException
	 *             if the class of an element of the specified collection
	 *             prevents it from being added to this list
	 * @throws NullPointerException
	 *             if the specified collection contains one or more null
	 *             elements and this list does not permit null elements, or if
	 *             the specified collection is null
	 * @throws IllegalArgumentException
	 *             if some property of an element of the specified collection
	 *             prevents it from being added to this list
	 * @see #add(Object)
	 */
	public boolean addAll(Collection<? extends E> c) {
		return linkedList.addAll(c);
	}

	/**
	 * Removes the first occurrence of the specified element from this list, if
	 * it is present (optional operation). If this list does not contain the
	 * element, it is unchanged. More formally, removes the element with the
	 * lowest index <tt>i</tt> such that
	 * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>
	 * (if such an element exists). Returns <tt>true</tt> if this list contained
	 * the specified element (or equivalently, if this list changed as a result
	 * of the call).
	 * 
	 * @param o
	 *            element to be removed from this list, if present
	 * @return <tt>true</tt> if this list contained the specified element
	 * @throws ClassCastException
	 *             if the type of the specified element is incompatible with
	 *             this list (<a
	 *             href="Collection.html#optional-restrictions">optional</a>)
	 * @throws NullPointerException
	 *             if the specified element is null and this list does not
	 *             permit null elements (<a
	 *             href="Collection.html#optional-restrictions">optional</a>)
	 * @throws UnsupportedOperationException
	 *             if the <tt>remove</tt> operation is not supported by this
	 *             list
	 */
	public boolean remove(E item) {
		return linkedList.remove(item);
	}

	/**
	 * Removes the element at the specified position in this list (optional
	 * operation). Shifts any subsequent elements to the left (subtracts one
	 * from their indices). Returns the element that was removed from the list.
	 * 
	 * @param index
	 *            the index of the element to be removed
	 * @return the element previously at the specified position
	 * @throws UnsupportedOperationException
	 *             if the <tt>remove</tt> operation is not supported by this
	 *             list
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (
	 *             <tt>index &lt; 0 || index &gt;= size()</tt>)
	 */
	public E remove(int index) {
		return linkedList.remove(index);
	}

	/**
	 * Returns the number of elements in this list. If this list contains more
	 * than <tt>Integer.MAX_VALUE</tt> elements, returns
	 * <tt>Integer.MAX_VALUE</tt>.
	 * 
	 * @return the number of elements in this list
	 */
	public int size() {
		return linkedList.size();
	}

	/**
	 * Removes all of the elements from this list (optional operation). The list
	 * will be empty after this call returns.
	 * 
	 * @throws UnsupportedOperationException
	 *             if the <tt>clear</tt> operation is not supported by this list
	 */
	public void clear() {
		linkedList.clear();
	}

	/**
	 * Returns the element at the specified position in this list. Or null if
	 * there is no element
	 * 
	 * @param index
	 *            index of the element to return
	 * @return the element at the specified position in this list or null if
	 *         there is no element
	 */
	public E get(int index) {
		E value;
		try {
			value = linkedList.get(index);
		} catch (IndexOutOfBoundsException e) {
			value = null;
		}
		return value;

	}

	/**
	 * Returns the element after item. If item is the last element it returns
	 * the first.
	 * 
	 * @param item
	 *            the element to get the successor from
	 * @return the element after item
	 */
	public E get_successor(E item) {
		E value = null;
		int index = linkedList.indexOf(item);

		if (index >= 0) {
			try {
				E tmp = linkedList.get(index + 1);
				value = tmp;
			} catch (IndexOutOfBoundsException e) {
				value = linkedList.get(0);
			}
		}
		return value;
	}

	/**
	 * Returns the element before item. If item is the first element it returns
	 * the last element.
	 * 
	 * @param item
	 *            the element to get the predecessor from
	 * @return the element before item
	 */
	public E get_predecessor(E item) {
		E value = null;
		int index = linkedList.indexOf(item);
		if (index >= 0) {
			try {
				E tmp = linkedList.get(index - 1);
				value = tmp;
			} catch (IndexOutOfBoundsException e) {
				value = linkedList.get(linkedList.size() - 1);
			}
		}
		return value;
	}

	/**
	 * Returns an iterator over the elements in this list in proper sequence.
	 * 
	 * @return an iterator over the elements in this list in proper sequence
	 */
	@Override
	public Iterator<E> iterator() {
		return linkedList.iterator();
	}

	/**
	 * Set a new actual item.
	 * 
	 * @param item
	 *            the new actual item.
	 */
	public void set_actual(E item) {
		this.actual = item;
	}

	/**
	 * Returns the element after the actual item. If item is the last element it
	 * returns the first.
	 * 
	 * @return the element after item
	 */
	public E get_successor() {
		return get_successor(actual);
	}

	/**
	 * Returns the element before the actual item. If item is the first element
	 * it returns the last element.
	 * 
	 * @return the element before item
	 */
	public E get_predecessor() {
		return get_predecessor(actual);
	}
}