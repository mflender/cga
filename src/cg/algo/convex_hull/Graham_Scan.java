/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.algo.convex_hull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.awt.Color;

import javax.swing.JOptionPane;

import cg.Drawing_Space;
import cg.algo.Algo;
import cg.drawable.Colored_line;
import cg.drawable.Colored_point;
import cg.drawable.Draw_Container;

/**
 * Performs a Graham Scan over a given set of points.
 * 
 * @author malte
 * 
 */
public class Graham_Scan extends Algo {
	private final int STEP_TIME = 500;
	private final int ANIM_COL_R = 255;
	private final int ANIM_COL_G = 0;
	private final int ANIM_COL_B = 0;

	private static enum Turn {
		CLOCKWISE, COUNTER_CLOCKWISE, COLLINEAR
	}

	@Override
	public void compute(Draw_Container data, Drawing_Space draw_panel) {

		data.get_lines().clear();

		if (data.get_points().size() < 3) {
			JOptionPane.showMessageDialog(null,
					"Can not compute Graham Scan, to few points.", "Error",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Graham_Scan.compute: to few points.");
			return;
		}

		List<Colored_point> sorted = new ArrayList<>(
				sort_points(data.get_points()));

		Stack<Colored_point> stack_lower = first_loop(data.get_lines(),
				draw_panel, sorted);

		Stack<Colored_point> stack_upper = second_loop(data.get_lines(),
				draw_panel, sorted);

		sorted.clear();

		List<Colored_point> l_upper = new ArrayList<>(stack_lower);
		List<Colored_point> l_lower = new ArrayList<>(stack_upper);

		sorted.addAll(l_upper);

		if (l_lower.size() > 0) {
			l_lower.remove(0);
			l_lower.remove(l_lower.size() - 1);
			// sorted.addAll(l_lower);
		}

		// So every point occurs just once only needed for collinar points
		for (int i = 0; i < l_lower.size(); i++) {
			if (!l_upper.contains(l_lower.get(i))) {
				sorted.add(l_lower.get(i));
			}
		}

		data.get_lines().clear();
		System.out.println("\n");

		// This need O(n) but is not part of the algo
		int j;
		for (j = 0; j < sorted.size(); j++) {
			System.out.println(sorted.get(j));
			sorted.get(j).set_color(
					new Color(ANIM_COL_R, ANIM_COL_G, ANIM_COL_B));
			try {
				data.get_lines().add(
						new Colored_line(sorted.get(j), sorted.get(j + 1),
								new Color(ANIM_COL_R, ANIM_COL_G, ANIM_COL_B)));
			} catch (IndexOutOfBoundsException e) {
			}
		}

		data.get_lines().add(
				new Colored_line(sorted.get(0), sorted.get(j - 1), new Color(
						ANIM_COL_R, ANIM_COL_G, ANIM_COL_B)));

		data.get_points().addAll(sorted);
	}

	/**
	 * Runs the first loop to get all lower points of the convex hull.
	 * 
	 * @param lines
	 *            the lines to animate the algo
	 * @param draw_panel
	 *            to draw the results on
	 * @param sorted
	 *            the sorted list of points
	 * @return a stack with all lower points
	 */
	private Stack<Colored_point> first_loop(List<Colored_line> lines,
			Drawing_Space draw_panel, List<Colored_point> sorted) {
		Stack<Colored_point> stack_lower = new Stack<>();
		stack_lower.push(sorted.get(0));
		stack_lower.push(sorted.get(1));

		for (int i = 2; i < sorted.size(); i++) {
			Colored_point head = sorted.get(i);
			Colored_point middle = stack_lower.pop();
			Colored_point tail = stack_lower.peek();

			middle.set_color(new Color(ANIM_COL_R, ANIM_COL_G, ANIM_COL_B));
			tail.set_color(new Color(ANIM_COL_R, ANIM_COL_G, ANIM_COL_B));

			lines.add(new Colored_line(head, middle, new Color(ANIM_COL_R,
					ANIM_COL_G, ANIM_COL_B)));
			lines.add(new Colored_line(middle, tail, new Color(ANIM_COL_R,
					ANIM_COL_G, ANIM_COL_B)));

			wait_paint(draw_panel, STEP_TIME);
			Turn turn = get_Turn(tail, middle, head);

			if (turn == Turn.CLOCKWISE) {
				i--;
				middle.set_color(new Color(0, 0, 0));
				int size = lines.size();
				for (int j = 1; j <= 4; j++) {
					lines.remove(size - j);
				}
			} else {
				stack_lower.push(middle);
				stack_lower.push(head);
			}
		}
		return stack_lower;
	}

	/**
	 * Runs the first loop to get all upper points of the convex hull.
	 * 
	 * @param lines
	 *            the lines to animate the algo
	 * @param draw_panel
	 *            to draw the results on
	 * @param sorted
	 *            the sorted list of points
	 * @return a stack with all upper points
	 */
	private Stack<Colored_point> second_loop(List<Colored_line> lines,
			Drawing_Space draw_panel, List<Colored_point> sorted) {
		Stack<Colored_point> stack_upper = new Stack<>();
		stack_upper.push(sorted.get(sorted.size() - 1));
		stack_upper.push(sorted.get(sorted.size() - 2));

		for (int i = sorted.size() - 3; i >= 0; i--) {
			Colored_point head = sorted.get(i);
			Colored_point middle = null;
			Colored_point tail = null;

			try {
				middle = stack_upper.pop();
				tail = stack_upper.peek();
			} catch (EmptyStackException e) {
				break;
			}

			middle.set_color(new Color(ANIM_COL_R, ANIM_COL_G, ANIM_COL_B));
			tail.set_color(new Color(ANIM_COL_R, ANIM_COL_G, ANIM_COL_B));

			lines.add(new Colored_line(head, middle, new Color(ANIM_COL_R,
					ANIM_COL_G, ANIM_COL_B)));
			lines.add(new Colored_line(middle, tail, new Color(ANIM_COL_R,
					ANIM_COL_G, ANIM_COL_B)));

			wait_paint(draw_panel, STEP_TIME);
			Turn turn = get_Turn(tail, middle, head);

			if (turn == Turn.CLOCKWISE) {
				i++;
				middle.set_color(new Color(0, 0, 0));
				int size = lines.size();
				for (int j = 1; j <= 4; j++) {
					lines.remove(size - j);
				}
			} else {
				stack_upper.push(middle);
				stack_upper.push(head);
			}
		}
		return stack_upper;
	}

	/**
	 * Sorts a set of points based on there angel with the smallest point. In
	 * O(n*log(n))
	 * 
	 * @param points
	 *            the set of points
	 * @return a well-ordered set of points
	 */
	private Set<Colored_point> sort_points(List<Colored_point> points) {

		// needs O(n)
		final Colored_point lowest = get_Lowest_Point(points);

		TreeSet<Colored_point> sort_set = new TreeSet<>(
				new Comparator<Colored_point>() {

					@Override
					public int compare(Colored_point a, Colored_point b) {

						if (a.equals(b) || a == b) {
							return 0;
						}

						// find the lowest angle to the lowest point
						double thetaA = Math.atan2((long) a.y - lowest.y,
								(long) a.x - lowest.x);
						double thetaB = Math.atan2((long) b.y - lowest.y,
								(long) b.x - lowest.x);

						if (thetaA < thetaB) {
							return -1;
						} else if (thetaA > thetaB) {
							return 1;
						} else {
							// get the length of the vector with pytagoras
							double distanceA = Math
									.sqrt((((long) lowest.x - a.x) * ((long) lowest.x - a.x))
											+ (((long) lowest.y - a.y) * ((long) lowest.y - a.y)));

							double distanceB = Math
									.sqrt((((long) lowest.x - b.x) * ((long) lowest.x - b.x))
											+ (((long) lowest.y - b.y) * ((long) lowest.y - b.y)));

							if (distanceA < distanceB) {
								return -1;
							} else {
								return 1;
							}
						}
					}
				});

		// needs O(log n)
		sort_set.addAll(points);

		return sort_set;
	}

	/**
	 * Returns the lexicographically smallest point of the set.
	 * 
	 * @param points
	 *            the set of points to check
	 * @return the smallest point
	 */
	private Colored_point get_Lowest_Point(List<Colored_point> points) {
		Colored_point lowest = points.get(0);
		for (int i = 1; i < points.size(); i++) {
			Colored_point tmp = points.get(i);
			// Sorts lexicographically
			if ((tmp.y < lowest.y) || (tmp.y == lowest.y && tmp.x < lowest.x)) {
				lowest = tmp;
			}
		}
		return lowest;
	}

	/**
	 * Checks of the three points perform a right or a left or no turn by
	 * computing the scalar product.
	 * 
	 * @param a
	 *            The first point
	 * @param b
	 *            The second point
	 * @param c
	 *            The third point
	 * @return CW CCW or collinear
	 */
	private Turn get_Turn(Colored_point a, Colored_point b, Colored_point c) {

		// vec_ab * vec_ac
		long cross_product = (((long) b.x - a.x) * ((long) c.y - a.y))
				- (((long) b.y - a.y) * ((long) c.x - a.x));

		if (cross_product > 0) {
			return Turn.COUNTER_CLOCKWISE;
		} else if (cross_product < 0) {
			return Turn.CLOCKWISE;
		} else {
			return Turn.COLLINEAR;
		}
	}

}