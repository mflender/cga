/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg;

import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import java.awt.Dimension;

import cg.drawable.Colored_point;
import cg.drawable.Colored_polygon;
import cg.drawable.Colored_polygon_self;
import cg.drawable.Draw_Container;

/**
 * The Parser to get the Points.
 * 
 * @author malte
 * 
 */
public class Parser {

	/**
	 * Parses a .obj file contend into a list of 2D objects.
	 * 
	 * @param contend
	 *            the string with the .obj file contend
	 * @param data
	 *            the 2D object container
	 */
	public void parse(String contend, Draw_Container data) {
		if (contend != null && !contend.equals("") && data.get_points() != null) {
			String[] lines = contend.split("\n");
			for (String line : lines) {
				if (line.startsWith("v")) {
					parse_Points(data, line);
				} else if (line.startsWith("f")) {
					pars_polygons_self(data, line);
					pars_polygons(data, line);
				}
			}
		} else {
			System.err.println("Parser.parse: no Data.");
		}
	}

	/**
	 * Parses a single line into a polygon.
	 * 
	 * NOTE: must be called after parse_Points() since it can only form a
	 * polygon out of already existing points.
	 * 
	 * 
	 * @param data
	 *            data the data to write the polygon into
	 * @param line
	 *            the line with the information of the polygon
	 */
	private void pars_polygons(Draw_Container data, String line) {
		String[] points_in_polygon = line.split(" ");
		Colored_polygon tmp = new Colored_polygon(new Color(0));
		for (int i = 1; i < points_in_polygon.length; i++) {
			try {
				int point_number = Integer.parseInt(points_in_polygon[i]);
				Colored_point point = data.get_points().get(point_number - 1);
				tmp.addPoint(point.x, point.y);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IndexOutOfBoundsException e) {
				System.err
						.println("invalid obj file first the vertex than the faces.");
			}
		}
		data.get_polygons().add(tmp);
	}

	/**
	 * Parses a single line into a point.
	 * 
	 * @param data
	 *            the data to write the point into
	 * @param line
	 *            the line with the information of the point
	 */
	private void parse_Points(Draw_Container data, String line) {
		String[] coords = line.split(" ");
		try {
			double x = Double.parseDouble(coords[1]);
			double y = Double.parseDouble(coords[2]);
			data.get_points().add(
					new Colored_point((int) x, (int) y, new Color(0, 0, 0)));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Parses a single line into a self made polygon.
	 * 
	 * NOTE: must be called after parse_Points() since it can only form a
	 * polygon out of already existing points.
	 * 
	 * 
	 * @param data
	 *            data the data to write the polygon into
	 * @param line
	 *            the line with the information of the polygon
	 */
	private void pars_polygons_self(Draw_Container data, String line) {
		String[] points_in_polygon = line.split(" ");
		Colored_polygon_self tmp = new Colored_polygon_self();
		for (int i = 1; i < points_in_polygon.length; i++) {
			try {
				int point_number = Integer.parseInt(points_in_polygon[i]);
				Colored_point point = data.get_points().get(point_number - 1);
				tmp.get_points().add(point);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IndexOutOfBoundsException e) {
				System.err
						.println("invalid obj file first the vertex than the faces.");
			}
		}
		tmp.create_lines();
		data.get_polygons_self().add(tmp);
	}

	/**
	 * Checks if a 2D object is beyond the screen range and deletes it, (0,0) is
	 * set as the min values.
	 * 
	 * @param screen_size
	 *            the bounds of the screen
	 * @param data
	 *            the 2D object container
	 */
	public void remove_objects(Dimension screen_size, Draw_Container data) {
		remove_points(screen_size, data);
		remove_polygons(screen_size, data);
		remove_polygons_self(screen_size, data);
	}

	/**
	 * Checks if a point is beyond the screen range and deletes it, (0,0) is set
	 * as the min values.
	 * 
	 * @param screen_size
	 *            the bounds of the screen
	 * @param data
	 *            the 2D object container
	 */
	private void remove_points(Dimension screen_size, Draw_Container data) {
		List<Colored_point> temp = new ArrayList<>();
		for (Colored_point point : data.get_points()) {
			if ((point.x < 0 || point.x > screen_size.height)
					|| (point.y < 0 || point.y > screen_size.width)) {
				temp.add(point);
			}
		}
		for (int i = 0; i < temp.size(); i++) {
			for (int j = 0; j < data.get_points().size(); j++) {
				if (temp.get(i).equals(data.get_points().get(j))) {
					data.get_points().remove(j);
				}
			}
		}
	}

	/**
	 * Checks if a polygon is beyond the screen range and deletes it, (0,0) is
	 * set as the min values.
	 * 
	 * @param screen_size
	 *            the bounds of the screen
	 * @param data
	 *            the 2D object container
	 */
	private void remove_polygons(Dimension screen_size, Draw_Container data) {
		List<Colored_polygon> temp = new ArrayList<>();
		for (Colored_polygon poly : data.get_polygons()) {
			if (poly.getBounds2D().getMinX() < 0
					|| poly.getBounds2D().getMinY() < 0
					|| poly.getBounds2D().getMaxX() > screen_size.height
					|| poly.getBounds2D().getMaxY() > screen_size.width) {
				temp.add(poly);
			}
		}
		for (int i = 0; i < temp.size(); i++) {
			for (int j = 0; j < data.get_polygons().size(); j++) {
				if (temp.get(i).equals(data.get_polygons().get(j))) {
					data.get_polygons().remove(j);
				}
			}
		}
	}

	/**
	 * Checks if a self made polygon is beyond the screen range and deletes it,
	 * (0,0) is set as the min values.
	 * 
	 * @param screen_size
	 *            the bounds of the screen
	 * @param data
	 *            the 2D object container
	 */
	private void remove_polygons_self(Dimension screen_size, Draw_Container data) {
		List<Colored_polygon_self> temp = new ArrayList<>();
		for (Colored_polygon_self poly : data.get_polygons_self()) {
			for (Colored_point point : poly.get_points()) {
				if ((point.x < 0 || point.x > screen_size.height)
						|| (point.y < 0 || point.y > screen_size.width)) {
					temp.add(poly);
				}
			}
		}
		for (int i = 0; i < temp.size(); i++) {
			for (int j = 0; j < data.get_polygons_self().size(); j++) {
				if (temp.get(i).equals(data.get_polygons_self().get(j))) {
					data.get_polygons_self().remove(j);
				}
			}
		}
	}

}