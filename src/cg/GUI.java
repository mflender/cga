/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import cg.algo.Algo;
import cg.algo.convex_hull.Graham_Scan;
import cg.algo.kdtree.KDTree;
import cg.algo.quadtree.QuadTree;
import cg.algo.scanline.Scanline_Convex_hull;
import cg.algo.polygon_triangulation.Poly_tri;

/**
 * Creates the GUI and reads a .obj file.
 * 
 * @author malte
 * 
 */
public class GUI {

	private Dimension screen_size = null;
	private Algo algo = new Graham_Scan();
	private final String algolist[] = { "Konvexe Huelle", "KD Tree",
			"Quad Tree", "Scanline", "Polygon Triangulierung"};

	/**
	 * Creates a new gui to draw the algos and buttons on.
	 * 
	 * @param screen_size_new
	 *            the size of the drawing space
	 */
	public GUI(Dimension screen_size_new) {
		this.screen_size = screen_size_new;
	}

	/**
	 * Starts the GUI to display the canvas.
	 */
	public void start_GUI() {
		final Drawing_Space draw_panel = new Drawing_Space(screen_size);
		final JFrame frame = new JFrame("Coga Algo");
		final JButton load_button = new JButton("Load obj");
		final JButton algo_button = new JButton("Algo");
		final JPanel button_panel = new JPanel();
		final JComboBox<String> algo_select = new JComboBox<>(algolist);
		
		algo_select.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<String> selectedChoice = (JComboBox<String>) e.getSource();
				String algorithem = (String) selectedChoice.getSelectedItem();

				if(algorithem.equals(algolist[0])){
					algo = new Graham_Scan();
				} else if(algorithem.equals(algolist[1])){
					algo = new KDTree();
				} else if (algorithem.equals(algolist[2])){
					algo = new QuadTree();
				} else if (algorithem.equals(algolist[3])){
					algo = new Scanline_Convex_hull();
				} else if (algorithem.equals(algolist[4])){
					algo = new Poly_tri();
				}
			}
		});

		load_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				draw_panel.clear_screen();
				draw_panel.create_draw_data(load_file());
				draw_panel.repaint();
			}
		});

		algo_button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (algo != null) {
					draw_panel.compute_algo(algo);
					draw_panel.repaint();
				} else {
					System.err
							.println("GUI.start_GUI.load_button: no Algo to perform");
				}
			}
		});

		button_panel.setBackground(Color.LIGHT_GRAY);
		button_panel.add(algo_select);
		button_panel.add(load_button);
		button_panel.add(algo_button);

		draw_panel.setPreferredSize(new Dimension(screen_size.width + 1,
				screen_size.height + 1));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.add(button_panel, BorderLayout.SOUTH);
		frame.add(draw_panel, BorderLayout.CENTER);
		frame.setSize(screen_size);
		frame.setLocationRelativeTo(null);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Reads a .obj into a string.
	 * 
	 * @return a the .obj file as a string.
	 */
	private String load_file() {
		JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"OBJ file", "obj");
		chooser.setFileFilter(filter);
		String contend = "";
		String temp = "";
		int return_val = chooser.showOpenDialog(null);

		if (return_val == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			if (file.getName().split("\\.")[1].equals("obj")) {
				try {
					BufferedReader b_reader = new BufferedReader(
							new FileReader(file));
					while ((temp = b_reader.readLine()) != null) {
						contend += temp + "\n";
					}
					b_reader.close();
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Could not open file",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null, "Unsupportet file",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(null, "Can not open file.", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		return contend;
	}
}
