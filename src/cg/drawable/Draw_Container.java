/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.drawable;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

/**
 * A data container to store everything the algo can compute and the panel can
 * draw.
 * 
 * @author malte
 * 
 */
public class Draw_Container {

	private List<Colored_point> points = new ArrayList<>();
	private List<Colored_line> lines = new ArrayList<>();
	private List<Colored_polygon> polygons = new ArrayList<>();
	private List<Colored_polygon_self> polygons_self = new ArrayList<>();

	/**
	 * returns the list of points.
	 * 
	 * @return the list of points
	 */
	public List<Colored_point> get_points() {
		return points;
	}

	/**
	 * Sets a new list of points.
	 * 
	 * @param points_new
	 *            the new list of points
	 */
	public void set_points(List<Colored_point> points_new) {
		this.points = points_new;
	}

	/**
	 * returns the list of lines.
	 * 
	 * @return the list of lines
	 */
	public List<Colored_line> get_lines() {
		return lines;
	}

	/**
	 * Sets a new list of lines.
	 * 
	 * @param lines_new
	 *            the new list of lines
	 */
	public void set_lines(List<Colored_line> lines_new) {
		this.lines = lines_new;
	}

	/**
	 * Returns the list of all polygons.
	 * 
	 * @return the list of polygons
	 */
	public List<Colored_polygon> get_polygons() {
		return polygons;
	}

	/**
	 * Sets a new list of polygons.
	 * 
	 * @param polygons_new
	 *            the new polygon list
	 */
	public void set_polygons(List<Colored_polygon> polygons_new) {
		this.polygons = polygons_new;
	}

	/**
	 * @return the polygons_self
	 */
	public List<Colored_polygon_self> get_polygons_self() {
		return polygons_self;
	}

	/**
	 * @param polygons_self
	 *            the polygons_self to set
	 */
	public void set_polygons_self(List<Colored_polygon_self> polygons_self) {
		this.polygons_self = polygons_self;
	}

	/**
	 * Cleans the screen.
	 */
	public void clear() {
		lines.clear();
		points.clear();
		polygons.clear();
		polygons_self.clear();
	}

	/**
	 * Draws all points on the screen.
	 * 
	 * @param g
	 *            the graphics to draw on
	 * @param y
	 *            the y dimension of the screen
	 */
	public void draw_points(Graphics g, int y) {
		if (points != null) {
			for (Colored_point point : points) {
				draw_point(g, point, y);
			}
		} else {
			System.err.println("Drawing_Space.draw_points: No points to Draw.");
		}
	}

	/**
	 * Draws a single point.
	 * 
	 * @param point
	 *            the point to draw.
	 * 
	 */
	private void draw_point(Graphics g, Colored_point point, int y) {
		if (point != null) {
			g.setColor(point.get_color());
			int x_pos = point.x - (point.get_dimension() / 2);
			int y_pos = y - (point.y + (point.get_dimension() / 2));
			g.fillOval(x_pos, y_pos, point.get_dimension(),
					point.get_dimension());
		} else {
			System.err.println("null point");
		}
	}

	/**
	 * Draws all lines on the screen.
	 * 
	 * @param g
	 *            the graphics to draw on
	 * @param y
	 *            the y dimension of the screen
	 */
	public void draw_lines(Graphics g, int y) {
		if (lines != null) {
			for (Colored_line line : lines) {
				draw_line(g, line, y);
			}
		} else {
			System.err.println("No lines to Draw.");
		}
	}

	/**
	 * Draws a line object to the canvas.
	 * 
	 * @param line
	 *            the line to draw.
	 */
	private void draw_line(Graphics g, Colored_line line, int y) {
		g.setColor(line.get_color());
		Graphics2D g2 = (Graphics2D) g;
		if (line.get_stroke()) {
			g2.setStroke(new BasicStroke(3));
		}
		g2.draw(new Line2D.Double(line.getX1(), y - line.getY1(), line.getX2(),
				y - line.getY2()));
	}

	/**
	 * Draws all polygons.
	 * 
	 * @param g
	 *            the graphics to draw on
	 * @param y
	 *            the y dimension of the screen
	 */
	public void draw_polygons(Graphics g, int y) {
		if (polygons != null) {
			for (Colored_polygon poly : polygons) {
				draw_polygon(g, poly, y);
			}
		} else {
			System.err.println("No polygons to Draw.");
		}
	}

	/**
	 * Draws a single polygon.
	 * 
	 * @param g
	 *            the graphics to draw on
	 * @param poly
	 *            the polygon
	 * @param y
	 *            the y dimension of the screen
	 */
	private void draw_polygon(Graphics g, Colored_polygon poly, int y) {
		Colored_polygon tmp = new Colored_polygon(poly.get_color());
		for (int i = 0; i < poly.npoints; i++) {
			tmp.addPoint(poly.xpoints[i], y - poly.ypoints[i]);
		}
		g.setColor(tmp.get_color());
		g.drawPolygon(tmp);
	}

	/**
	 * Draws all polygons.
	 * 
	 * @param g
	 *            the graphics to draw on
	 * @param y
	 *            the y dimension of the screen
	 */
	public void draw_polygons_self(Graphics g, int y) {
		if (polygons_self != null) {
			for (Colored_polygon_self poly : polygons_self) {
				draw_polygon_self(g, poly, y);
			}
		} else {
			System.err.println("No polygons_self to Draw.");
		}
	}

	/**
	 * Draws a single polygon.
	 * 
	 * @param g
	 *            the graphics to draw on
	 * @param poly
	 *            the polygon
	 * @param y
	 *            the y dimension of the screen
	 */
	private void draw_polygon_self(Graphics g, Colored_polygon_self poly, int y) {
		for (Colored_line line : poly.get_lines()) {
			draw_line(g, line, y);
		}
		for (Colored_point point : poly.get_points()) {
			draw_point(g, point, y);
		}
	}

	@Override
	public String toString() {
		return "Draw_Container \npoints=" + points + "\nlines=" + lines
				+ "\npolygons=" + polygons;
	}

}