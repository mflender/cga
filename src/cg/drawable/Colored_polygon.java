/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.drawable;

import java.awt.Color;
import java.awt.Polygon;

/**
 * A polygon with additional color information. So the algo can change the color
 * of this polygon.
 * 
 * @author malte
 * 
 */
public class Colored_polygon extends Polygon {

	/**
	 * The serial ID.
	 */
	private static final long serialVersionUID = -117448266932135434L;
	private Color color = null;

	/**
	 * The standart constructor of a colored polygon.
	 * 
	 * @param color_new the color of the polygon
	 */
	public Colored_polygon(Color color_new) {
		super();
		this.color = color_new;
	}

	/**
	 * Gives the actual color back.
	 * 
	 * @return the color of the point
	 */
	public Color get_color() {
		return color;
	}

	/**
	 * Sets a new color to the point.
	 * 
	 * @param color_new
	 *            the new color
	 */
	public void set_color(Color color) {
		this.color = color;
	}
}