/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.drawable;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * A line with additional color. So the algo can change the color of the line.
 * 
 * @author malte
 * 
 */
public class Colored_line extends Line2D.Double {
	/**
	 * The serial ID
	 */
	private static final long serialVersionUID = -1957233005682825845L;
	private Color color = null;
	private boolean stroke = false;

	/**
	 * Crates a new line from 2 (x,y) positions and a color.
	 * 
	 * @param x1
	 *            the x position of the first dot
	 * @param y1
	 *            the y position of the first dot
	 * @param x2
	 *            the x position of the second dot
	 * @param y2
	 *            the y position of the second dot
	 * @param color_new
	 *            the color of the line
	 */
	public Colored_line(double x1, double y1, double x2, double y2,
			Color color_new) {
		super(x1, y1, x2, y2);
		this.color = color_new;
	}

	/**
	 * Crates a new line from 2 points positions and a color.
	 * 
	 * @param p1
	 *            the first point
	 * @param p2
	 *            the second point
	 * @param color_new
	 *            the color of the line
	 */
	public Colored_line(Point2D p1, Point2D p2, Color color_new) {
		super(p1, p2);
		this.color = color_new;
	}

	/**
	 * Gives the actual color back.
	 * 
	 * @return the color of the point
	 */
	public Color get_color() {
		return color;
	}

	/**
	 * Sets a new color to the point.
	 * 
	 * @param color_new
	 *            the new color
	 */
	public void set_color(Color color_new) {
		this.color = color_new;
	}

	@Override
	public String toString() {
		return "Colored_line [X1=" + super.x1 + ", Y1="
				+ super.y1 + ", X2=" + super.x2 + ", Y2=" + super.y2 +", color=" + color + "]";
	}

	/**
	 * Checks if this line should be painted more bold. 
	 * 
	 * @return stroke
	 */
	public boolean get_stroke() {
		return stroke;
	}

	/**
	 * Set the stroke witch draws this particular line a little more bold than
	 * the others.
	 * 
	 * @param stroke true if the line should be more bold.
	 */
	public void set_stroke(boolean stroke) {
		this.stroke = stroke;
	}

}
