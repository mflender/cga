/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.drawable;

import java.awt.Point;
import java.awt.Color;

/**
 * A point with additional color. So the algo can change the color of the point.
 * 
 * @author malte
 * 
 */
public class Colored_point extends Point {
	/**
	 * The serial ID.
	 */
	private static final long serialVersionUID = -1219086312160888455L;
	private Color color = null;
	private final int dot_dimension = 8;
	private Colored_line line_end_here = null;
	private Colored_line line_start_here = null;
	private boolean used = false;
	/**
	 * Crates a new point with additional color information.
	 * 
	 * @param x
	 *            the x coordinate
	 * @param y
	 *            the y coordinate
	 * @param color_new
	 *            the additional color
	 */
	public Colored_point(int x, int y, Color color_new) {
		super(x, y);
		this.set_color(color_new);
	}

	/**
	 * Gives the actual color back.
	 * 
	 * @return the color of the point
	 */
	public Color get_color() {
		return color;
	}

	/**
	 * Sets a new color to the point.
	 * 
	 * @param color_new
	 *            the new color
	 */
	public void set_color(Color color_new) {
		this.color = color_new;
	}

	/**
	 * returns the diameter of a single dot.
	 * 
	 * @return the diameter.
	 */
	public int get_dimension() {
		return dot_dimension;
	}

	@Override
	public String toString() {
		return "Colored_point [X=" + super.x + ", Y=" + super.y + ", color="
				+ color + "]";
	}

	/**
	 * @return the line_end_here
	 */
	public Colored_line get_line_end_here() {
		return line_end_here;
	}

	/**
	 * @param line_end_here the line_before to set
	 */
	public void set_line_end_here(Colored_line line_before) {
		this.line_end_here = line_before;
	}

	/**
	 * @return the line_start_here
	 */
	public Colored_line get_line_start_here() {
		return line_start_here;
	}

	/**
	 * @param line_start_here the line_after to set
	 */
	public void set_line_start_here(Colored_line line_after) {
		this.line_start_here = line_after;
	}

	/**
	 * @return the used
	 */
	public boolean get_Used() {
		return used;
	}

	/**
	 * @param used the used to set
	 */
	public void set_Used(boolean used) {
		this.used = used;
	}

}