/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg.drawable;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Colored_polygon_self {
	private List<Colored_point> points = new ArrayList<>();
	private List<Colored_line> lines = new ArrayList<>();
	private Colored_polygon poly_rep = new Colored_polygon(new Color(0, 0, 0));

	/**
	 * @return the points
	 */
	public List<Colored_point> get_points() {
		return points;
	}

	/**
	 * @param points
	 *            the points to set
	 */
	public void set_points(List<Colored_point> points) {
		this.points = points;
	}

	/**
	 * @return the lines
	 */
	public List<Colored_line> get_lines() {
		return lines;
	}

	/**
	 * @param lines
	 *            the lines to set
	 */
	public void set_lines(List<Colored_line> lines) {
		this.lines = lines;
	}

	/**
	 * Sets a new Color for the whole polygon.
	 * 
	 * @param color
	 *            the new color.
	 */
	public void set_color(Color color) {
		for (Colored_point point : points) {
			point.set_color(color);
		}
		for (Colored_line line : lines) {
			line.set_color(color);
		}
	}

	/**
	 * Creates the lines out of the existing points
	 */
	public void create_lines() {
		int i;
		for (i = 0; i < points.size(); i++) {
			try {
				Colored_point actual = points.get(i);
				Colored_point next = points.get(i + 1);
				Colored_line tmp = new Colored_line(actual, next,
						actual.get_color());
				actual.set_line_start_here(tmp);
				next.set_line_end_here(tmp);
				lines.add(tmp);
			} catch (IndexOutOfBoundsException e) {
			}
		}
		Colored_point last = points.get(i - 1);
		Colored_point first = points.get(0);
		Colored_line tmp = new Colored_line(last, first, last.get_color());
		last.set_line_start_here(tmp);
		first.set_line_end_here(tmp);
		lines.add(tmp);

	}

	/**
	 * Returns the point at the end of the given line.
	 * 
	 * @param line
	 *            the line to get the point from
	 * @return null or the point at the end of the line
	 */
	public Colored_point get_end_point_from_line(Colored_line line) {
		int x_2 = (int) line.getX2();
		int y_2 = (int) line.getY2();

		for (Colored_point point : points) {
			if (point.x == x_2 && point.y == y_2) {
				return point;
			}
		}
		return null;
	}

	/**
	 * returns the point at the start of the given line.
	 * 
	 * @param line
	 *            the line to get the point from
	 * @return null or the point at the start of the line
	 */
	public Colored_point get_start_point_from_line(Colored_line line) {
		int x_1 = (int) line.getX1();
		int y_1 = (int) line.getY1();

		for (Colored_point point : points) {
			if (point.x == x_1 && point.y == y_1) {
				return point;
			}
		}
		return null;
	}

	/**
	 * @return the poly_rep
	 */
	public Colored_polygon get_poly_rep() {
		poly_rep.reset();
		for (Colored_line line : lines) {
			poly_rep.addPoint((int) line.x1, (int) line.y1);
			poly_rep.addPoint((int) line.x2, (int) line.y2);
		}
		return poly_rep;
	}

	/**
	 * @param poly_rep
	 *            the poly_rep to set
	 */
	public void set_poly_rep(Colored_polygon poly_rep) {
		this.poly_rep = poly_rep;
	}

	/**
	 * Removes all used points from the Polygon.
	 */
	public void remove_used() {
		List<Colored_point> points_rem = new ArrayList<>();

		for (Colored_point point : points) {
			if (point.get_Used()) {
				points_rem.add(point);
			}
		}
		points.removeAll(points_rem);
	}

	/**
	 * Clears all pointers in the points.
	 */
	public void clear_point_pointer() {
		for (Colored_point point : points) {
			point.set_line_end_here(null);
			point.set_line_start_here(null);
		}

	}

	/**
	 * Decouples the start/end point of this polygon from the old one by
	 * creating new objects.
	 * 
	 * @param start_point
	 *            the start point to replace
	 * @param end_point
	 *            the endpoint to replace
	 */
	public void decuple_from_old(Colored_point start_point, Colored_point end_point) {
		Colored_point start_point_new = new Colored_point(start_point.x, start_point.y, start_point.get_color());
		points.set(points.lastIndexOf(start_point), start_point_new);

		Colored_point end_point_new = new Colored_point(end_point.x, end_point.y, end_point.get_color());
		points.set(points.lastIndexOf(end_point), end_point_new);
	}

}