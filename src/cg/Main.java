/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg;

import java.awt.Dimension;

/**
 * This Program shows a x,y canvas and loads some points from a .obj file to
 * display them.
 * 
 * @author malte
 * 
 */
public final class Main {

	private Main() {
	}

	/**
	 * Start the program.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		GUI gui = new GUI(new Dimension(500, 500));
		gui.start_GUI();
	}
}