/*
 * Copyright 2014 Malte Flender
 *
 * This file is part of a Malte project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package cg;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cg.algo.Algo;
import cg.drawable.Draw_Container;

/**
 * Prints a x,y spaced canvas and some dots on it.
 * 
 * @author malte
 * 
 */
public class Drawing_Space extends JPanel {
	/**
	 * The Serial ID.
	 */
	private static final long serialVersionUID = -5554714108890812792L;
	private Dimension screen_size = null;
	private Draw_Container draw_data = null;

	/**
	 * Creates a new space to draw the algos on.
	 * 
	 * @param screen_size_new
	 *            the size of the new space
	 */
	public Drawing_Space(Dimension screen_size_new) {
		draw_data = new Draw_Container();
		this.screen_size = screen_size_new;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.WHITE);
		draw_data.draw_polygons_self(g, screen_size.width);
		draw_data.draw_polygons(g, screen_size.width);
		draw_data.draw_points(g, screen_size.width);
		draw_data.draw_lines(g, screen_size.width);
	}

	/**
	 * Creates drawable data out of the .obj contend string.
	 * 
	 * @param contend
	 *            the contend of the .obj file
	 */
	public void create_draw_data(String contend) {
		if (contend != null) {
			Parser parser = new Parser();
			parser.parse(contend, draw_data);
			parser.remove_objects(screen_size, draw_data);
		} else {
			System.err.println("Drawing_Space.create_draw_data: no contend.");
		}
	}

	/**
	 * Clears the screen.
	 */
	public void clear_screen() {
		draw_data.clear();
	}

	/**
	 * Computes the Algo and draws the result.
	 * 
	 * @param algo
	 *            the algorithm to perform.
	 */
	public void compute_algo(Algo algo) {
		if (!draw_data.get_points().isEmpty()) {
			algo.compute(draw_data, this);
		} else {
			JOptionPane.showMessageDialog(null, "no .obj to perform Algo on.",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}